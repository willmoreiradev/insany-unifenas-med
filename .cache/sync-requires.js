const { hot } = require("react-hot-loader/root")

// prefer default export if available
const preferDefault = m => m && m.default || m


exports.components = {
  "component---cache-dev-404-page-js": hot(preferDefault(require("/Users/leonardopadua/Documents/projetos/insany/insany-unifenas-med/.cache/dev-404-page.js"))),
  "component---src-pages-alfenas-js": hot(preferDefault(require("/Users/leonardopadua/Documents/projetos/insany/insany-unifenas-med/src/pages/alfenas.js"))),
  "component---src-pages-bh-js": hot(preferDefault(require("/Users/leonardopadua/Documents/projetos/insany/insany-unifenas-med/src/pages/bh.js"))),
  "component---src-pages-index-js": hot(preferDefault(require("/Users/leonardopadua/Documents/projetos/insany/insany-unifenas-med/src/pages/index.js")))
}

