import React, { Component } from 'react'
import IconFechaModal from '../../images/close-modal.svg'
import ilustraSucesso from '../../images/ilustra-mensagem.png'

export default class ModalContato extends Component {
    checkForm = (e) => {
        e.preventDefault();
        let form = document.getElementById('contato-form');
        let modalForm = document.getElementsByClassName('modal-form')[0];
        let modalSucesso = document.getElementsByClassName('modal-sucesso')[0];
        let isValidForm = form.checkValidity();
        if(isValidForm){
            modalForm.style.display = 'none';
            modalSucesso.style.display = 'flex';
        }
    }
    showSucesso = (e) => {
        e.preventDefault();
    }
    openModal = (e) => {
        e.preventDefault();
        document.documentElement.classList.toggle('modal-contato-opened');  
    }
    render() {
        return (
            <div className="modal-contato">
                <div className="modal-contato-container">
                    <div className="modal-form">
                        <h1 className="modal-titulo">
                            Como podemos ajudar você?
                        </h1>
                        <form id="contato-form">
                            <div className="form-group">
                                <label>Nome</label>
                                <input type="text" className="form-control input-lg" placeholder="Seu Nome" name="nome" required />
                            </div>
                            <div className="form-group">
                                <label>E-mail</label>
                                <input type="email" className="form-control input-lg" placeholder="Seu E-mail" name="email" required />
                            </div>
                            <div className="form-group mensagem">
                                <label>Mensagem</label>
                                <textarea type="text" className="form-control input-lg" placeholder="Sua Mensagem" name="mensagem" required/>
                            </div>
                            <input type="submit" className="enviar" value="Enviar" onClick={this.checkForm}>
                            </input>
                        </form>
                    </div>
                    <div className="modal-sucesso">
                        <img src={ilustraSucesso} alt=""/>
                        <div className="texto">
                            <h1>A sua mensagem foi enviada!</h1>
                            <span>Vamos retornar o contato em breve 🙂</span>
                        </div>
                        <a className="btn" onClick={this.openModal}>
                            <span>
                                Voltar
                            </span>
                        </a>
                    </div>
                    <a className="close-modal-contato" onClick={this.openModal} >
                        <img src={IconFechaModal} />
                    </a>
                </div>
            </div>
        )
    }
}
