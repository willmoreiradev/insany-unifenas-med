import React, { Component } from 'react'

import tourImg from '../../images/tour-image.jpg';
import logoIcon from './../../images/logo-icon.svg';

export default class Tour extends Component {
    render() {
        return (
            <section className="s-tour">
                <div className="container">
                    <div className="texto">
                        <h1>Tour virtual no Campus</h1>
                        <p>Preparamos um vídeo apresentando um pouquinho da MED UNIFENAS pra você.</p>
                    </div>
                    <div className="tour">
                        {this.props.cidade == 'bh' ? 
                            <iframe src="https://www.youtube.com/embed/lgIKV7aBSHI" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                            :
                            <iframe src="https://www.youtube.com/embed/BFZRYlgluM4" frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
                        }
                    </div>
                </div>
            </section>
        )
    }
}
