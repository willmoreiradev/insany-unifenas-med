import React, { Component } from 'react';

import IconFacebook from '../../images/facebook.svg';
import IconInstagram from '../../images/instagram.svg';
import IconTwitter from '../../images/twiiter.svg';
import IconYoutube from '../../images/youtube.svg';
import IconeLapis from '../../images/lapis.svg';
import IconeDownload from '../../images/icone-download.svg';
import IconeLinkedin from '../../images/icon-linkedin-white.svg'

class SectionHistoria extends Component {
    render() {
        return (
            <section className="s-historia">
                <div className="container">
                    <div className="texto">
                        <h1>Venha ser MED UNIFENAS e construa sua história com a gente  ❤️</h1>
                        <p>Há mais de <strong>30 anos</strong> formando os melhores médicos do país.</p>
                        <div className="social">
                            <span>Nos siga nas redes sociais</span>
                            <ul>
                                <li>
                                    <a href="https://www.facebook.com/Unifenas" target="_blank">
                                        <img src={IconFacebook} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/unifenas" target="_blank">
                                        <img src={IconInstagram} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/unifenasbr" target="_blank">
                                        <img src={IconTwitter} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.youtube.com/user/unifenasbr" target="_blank">
                                        <img src={IconYoutube} alt="" />
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/school/15093246" target="_blank">
                                        <img src={IconeLinkedin} alt="" />
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className="btns">
                        <a href="http://inscricao.unifenas.br/index.aspx?v=VEST202MED" target="_blank" >
                            <div className="icone">
                                <img src={IconeLapis} alt="" />
                            </div>
                            <span>Inscreva-se no Vestibular</span>
                        </a>
                        <a href="http://www.unifenas.br/vestibular/20202/edital_medicina.pdf" target="_blank" >
                            <div className="icone">
                                <img src={IconeDownload} alt="" />
                            </div>
                            <span>Baixe o edital</span>
                        </a>
                        <a href="http://www.unifenas.br/vestibular/20202/manual_medicina.pdf" target="_blank" >
                            <div className="icone">
                                <img src={IconeDownload} alt="" />
                            </div>
                            <span>Baixe o manual</span>
                        </a>
                    </div>
                </div>
            </section>
        );
    }
}

export default SectionHistoria;