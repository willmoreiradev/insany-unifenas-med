import React, { Component } from 'react';

import Styled from 'styled-components';

const StyledBtn = Styled.a`
    width: 289px;
    height: 60px;
    border-radius: 6px;
    background-color: ${props => props.disabled ? '#c4c4c4' : '#f3b502'};
    display: flex;
    pointer-events: all;
    justify-content: center;
    align-items: center;
    color: #ffffff;
    font-family: 'DINPro';
    font-size: 16px;
    font-weight: 700;
    transform: scale(1);
    transition: all .3s;
    opacity: ${props => props.disabled ? '.5' : '1'}
    cursor: ${props => props.disabled ? 'default' : 'pointer'};
    z-index: 2;
    &:hover {
        ${props => props.disabled ? 'scale(1)' : 'scale(1.05)'};
        transition: all .3s;
    }
`


class BtnAmarelo extends Component {
    render() { 
        return ( 
            <StyledBtn href={this.props.link} target="_blank" disabled={this.props.disabled} className={this.props.isHorarios ? "botaoAmareloH botaoAmarelo" : "botaoAmarelo"}>
                {this.props.disabled ? 'Em Breve' : this.props.text}
            </StyledBtn>
        );
    }
}
 
export default BtnAmarelo;