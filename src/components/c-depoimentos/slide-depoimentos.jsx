import React from 'react';
import Swiper from 'react-id-swiper';

const slideDepoimentos = (props) => {
    const params = {
        navigation: {
            nextEl: '.ctrl-slide-depo .btn-next',
            prevEl: '.ctrl-slide-depo .btn-prev'
        },
        slidesPerView: 'auto',
        speed: 800,
        spaceBetween: 30,
    }
    let depoimentos = []
    depoimentos = props.depoimentos

    return (
        <Swiper {...params}>
            {
            depoimentos.map((depoimento, i) => (
                depoimento ? (
                <div key={i}>
                    <div className="box-depoimento">
                        <div className="foto">
                            <img src={depoimento.avatar} alt="" />
                        </div>
                        <p>
                            {depoimento.texto}
                        </p>
                        <div className="pessoa">
                            <strong>{depoimento.nome}</strong>
                            <span>{depoimento.descricao}</span>
                        </div>
                    </div>
                </div>
                ) : null
            ))
            }
        </Swiper>
    )
}

export default slideDepoimentos;