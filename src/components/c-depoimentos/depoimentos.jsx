import React, { Component } from 'react';

import SlideDepoimentos from './slide-depoimentos';

import { Parallax } from 'react-scroll-parallax';

import ArrowPrev from '../../images/arrow-prev-depo.svg';
import ArrowNext from '../../images/arrow-next-depo.svg';
import ImgVirus from '../../images/img_virus.png';
import Cerebro from '../../images/img_human_brain.png';
import Sangue from '../../images/img_blood_cells.png';


class SectionDepoimentos extends Component {
    render() { 
        return ( 
            <section className="s-depoimentos">
                <div className="container">
                    <Parallax className="custom-class" y={[30, -80]} tagOuter="figure" className="virus">
                        <img src={ImgVirus} />
                    </Parallax>   
                    <Parallax className="custom-class" y={[120, -120]} tagOuter="figure" className="cerebro">
                        <img src={Cerebro} />
                    </Parallax>        
                    <div className="topo">
                        <h1>Pra gente a sua opinião é muito importante</h1>
                        <div className="ctrl-slide-depo ctrl-slide">
                            <img src={ArrowPrev} className="btn btn-prev" alt=""/>
                            <img src={ArrowNext} className="btn btn-next" alt=""/>
                        </div>
                    </div>
                    <SlideDepoimentos depoimentos={this.props.depoimentos} />     
                    <Parallax className="custom-class" y={[200, -50]} tagOuter="figure" className="blood">
                        <img src={Sangue} />
                    </Parallax>           
                </div>
            </section>
        );
    }
}
 
export default SectionDepoimentos;