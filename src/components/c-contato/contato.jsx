import React, { Component } from 'react'
import styled from 'styled-components'

import iconWhatsapp from '../../images/icon-wpp.svg'
import iconEmail from '../../images/icon-email.svg'
import iconPhone from '../../images/icone-phone.svg'
import circlesIlustra from '../../images/ilustra-fc_02-circulo.svg'
import logoFacebook from '../../images/logo-facebook.svg'
import logoInsta from '../../images/logo-insta.svg'
import logoTwitter from '../../images/logo-twitter.svg'
import logoYoutube from '../../images/logo-youtube.svg'
import logoLinkedin from '../../images/icon_linkedin.svg'
import setaBaixo from '../../images/seta-baixo.svg';
 

const BtnContato = styled.a`
    cursor: pointer;
    width: 200px;
    height: 128px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin-right: 10px;
    margin-left: 10px;
    box-shadow: 0 2px 16px rgba(15, 83, 138, 0.12);
    border-radius: 6px;
    background-color: #ffffff;
    transition: all .4s;
    span{
        margin-top: 1rem;
        color: #888593;
        font-size: 15px;
        font-weight: 400;
    }

    &:hover{
        background-color: #00abc5;
        span{
            color: #ffffff;
        }
        transition: all .4s;
        img{
            filter: brightness(10) grayscale(1);
        }
    }
    @media (max-width: 1050px) {
        width: 160px;
        height: 160px;
    }
    @media (max-width: 480px) {
        width: 100px;
        height: 100px;
    }
`;

const ImgContainer = styled.div`
    height: 511px;
    width: 592px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    background-size: contain;


    @keyframes pulse {
        0% {
            transform: scale(0.95);
        }

        70% {
            transform: scale(1);
        }

        100% {
            transform: scale(0.95);
        }
    }

    .circles {
        position: absolute;
        top: 0;
        z-index: -1;
        animation: pulse 5s infinite;
    }

    &:after {
        content: "";
        background: url(${props => (props.imgAfter ? props.imgAfter : null)}) no-repeat center center;
        position: absolute;
        top: ${props => (props.topAfter ? props.topAfter : null)}px;
        right: 0;
        width: 100%;
        height: 100%;
        background-size: contain;
        pointer-events: none;
    }
    &:before {
        content: "";
        background: url(${props => (props.imgBefore ? props.imgBefore : null)}) no-repeat center center;
        position: absolute;
        top: 8px;
        right: 0;
        width: 100%;
        height: 100%;
        background-size: contain;
        pointer-events: none;
    }
    @media (max-width: 1050px) {
        img{
            width: 100%;
        }
        width: 282px;
        height: 276px;
        &:after {
            top: 0;
        }
        &:before {
            top: 0;
        }
    }
`;

const LinksContato = styled.div`
    .contato{
        display: flex;
        align-items: center;
        justify-content: flex-start;
        margin-bottom: 18px;
        height: 30px;
    }
    .icon{
        width: 20px;
        margin-right: 12px;
        display: flex;
        align-items: center;
        justify-content: center;
        margin-right: 12px;
        img{
            width: 100%;
        }
    }
    a{
        color: #0f538a;
        font-family: "DINPro";
        font-size: 16px;
        font-weight: 700;
        line-height: 27px;
    }
`;

export default class Contato extends Component {
    openModal = (e) => {
        e.preventDefault();
        document.documentElement.classList.toggle('modal-contato-opened');  
    }
    render() {
        return (
            <section className="s-contato">
                <div className="container">
                    <div className="text">
                        <h1>
                            Fale conosco
                        </h1>
                        <p>
                            Quer enviar uma mensagem, tirar alguma dúvida ou bater um papo? 
                        </p>
                        <div className="btn-contato-container">
                            <BtnContato href={this.props.whatsapp} target="_blank">
                                    <img src={iconWhatsapp} alt=""/>
                                    <span>
                                        Whatsapp
                                    </span>
                            </BtnContato >

                        </div>
                        <LinksContato>
                            <div className="contato">
                                <div className="icon">
                                    <img src={iconEmail} alt="icon-email" />
                                </div>
                                <a href={`mailto:${this.props.email}`} target="_blank">{this.props.email}</a>
                            </div>
                            <div className="contato">
                                <div className="icon">
                                    <img src={iconPhone} alt="icon-telefone" />
                                </div>
                                <a href={`tel:${this.props.actionTel}`} target="_blank">{this.props.telefone}</a>
                            </div>
                        </LinksContato>
                        <div className="redes-sociais-container">
                            <p>Estamos nas redes sociais também 😉</p>

                            <div className="redes-sociais">
                                <a href="https://www.facebook.com/Unifenas" target="_blank" className="rede-social">
                                    <img src={logoFacebook} alt=""/>
                                </a>
                                <a href="https://www.instagram.com/unifenas" target="_blank" className="rede-social">
                                    <img src={logoInsta} alt=""/>
                                </a>
                                <a href="https://twitter.com/unifenasbr" target="_blank" className="rede-social">
                                    <img src={logoTwitter} alt=""/>
                                </a>
                                <a href="https://www.youtube.com/user/unifenasbr" target="_blank" className="rede-social">
                                    <img src={logoYoutube} alt=""/>
                                </a>
                                <a href="https://www.linkedin.com/school/15093246" target="_blank" className="rede-social">
                                    <img src={logoLinkedin} alt=""/>
                                </a>
                            </div>
                        </div>
                        <a href="#mapa" className="como-chegar">
                            <img src={setaBaixo} alt=""/>
                            <span >Saiba como chegar</span>
                        </a>
                    </div>
                    <ImgContainer imgAfter={this.props.imgAfter} topAfter={this.props.topAfter} imgBase={this.props.imgBase} imgBefore={this.props.imgBefore} >
                        <img src={circlesIlustra} className="circles" alt=""/>
                        <img src={this.props.img} alt=""/>
                    </ImgContainer>
                </div>  
            </section>
        )
    }
}
