import React, { Component } from 'react'
import styled from 'styled-components';

import { Parallax } from 'react-scroll-parallax';

import logoIcon from '../../images/logo-icon.svg';
import iconPencil from '../../images/icon-pencil.svg';
import iconDownload from '../../images/icon-download.svg';



const ImgContainer = styled.div`
    height: 511px;
    width: 592px;
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    img{
        position: absolute;
    }
    .bola{
        width: 20px;
        height: 20px;
        border-radius: 21px;
        box-shadow: 0px 6px 18px #0F538A;
    }
    .bolas-cinza{
        position: absolute;
        margin-left: -296px;
        left: 50%;
        width: 592px;
        height: auto;
        z-index: 2;
        top: 13px;
    }
    .azul-esq-top{
        width: 12px;
        height: 12px;
        border-radius: 12px;
        position: absolute;
        left: ${props => props.cidade == 'al' ? '6px': '528px'};
        bottom: ${props => props.cidade == 'al' ? '263px': '475px'};
        box-shadow: 0px 6px 18px #0F538A;
        z-index: 3;
    }
    .azul-dir-top{
        right: ${props => props.cidade == 'al' ? '59px': '467px'};
        position: absolute;
        top: ${props => props.cidade == 'al' ? '31px': '114px'};
        z-index: 3;
    }
    .azul-esq-down{
        position: absolute;
        left: ${props => props.cidade == 'al' ? '100px': '452px'};
        bottom: ${props => props.cidade == 'al' ? '-18px': '-32px'};;
        z-index: 3;
    }
    .cards{
        max-width: 100%;
        left: 50%;
        margin-left: -292px;
        top: -30px;
        z-index: 2;
    }
    .principal{
        margin-left: -296px;
        left: 50%;
        width: 592px;
        height: auto;
        z-index: 1;
    }
    @media (max-width: 1050px) {
        height: 302px;
        width: 341px;
        img{
            position: absolute;
        }
        .bola{
            width: 12px;
            height: 12px;
            border-radius: 11px;
            box-shadow: 0px 6px 18px #0F538A;
        }
        .bolas-cinza{
            display: none
        }
        .azul-esq-top{
            width: 12px;
            height: 12px;
            border-radius: 12px;
            position: absolute;
            left: ${props => props.cidade == 'al' ? '40px': '279px'};
            bottom: ${props => props.cidade == 'al' ? '247px': '245px'};
            box-shadow: 0px 6px 18px #0F538A;
            z-index: 3;
        }
        .azul-dir-top{
            right: ${props => props.cidade == 'al' ? '59px': '256px'};
            position: absolute;
            top: ${props => props.cidade == 'al' ? '31px': '70px'};
            z-index: 3;
        }
        .azul-esq-down{
            position: absolute;
            left: ${props => props.cidade == 'al' ? '86px': '212px'};
            bottom: ${props => props.cidade == 'al' ? '9px': '18px'};;
            z-index: 3;
        }
        .cards{
            max-width: 100%;
            left: 50%;
            margin-left: -171px;
            top: 0px;
            z-index: 2;
        }
        .principal{
            margin-left: -143px;
            left: 50%;
            width: 286px;
            height: auto;
            z-index: 1;
        }
    }
`;

export default class UniCompleta extends Component {
    render() {
        return (
            <section className="s-uni-completa">
                <div className="container">
                    <div className="texto">
                        {this.props.logo ? <img src={logoIcon} /> : null}
                        <h1>{this.props.titulo}</h1>
                        <p>{this.props.texto}</p>
                        <div className="btn-container">
                            <a className="btn" href="http://inscricao.unifenas.br/index.aspx?v=VEST202MED" target="_blank">
                                <img className="pencil" src={iconPencil} />    
                                Inscreva-se 
                            </a>
                            <a className="btn" href="http://www.unifenas.br/vestibular/20202/edital_medicina.pdf" target="_blank">
                                <img className="download" src={iconDownload} />    
                                Baixe o edital
                            </a>
                        </div>
                    </div>
                    <ImgContainer cidade={this.props.cidade}>
                         {this.props.azulEsqDown ?
                            <Parallax className="bola azul-esq-down" y={[-20, 20]} tagOuter="figure">
                                <img src={this.props.azulEsqDown} alt=""/> 
                            </Parallax>
                        : null}
                        {this.props.azulEsqTop ?
                            <Parallax className="bola azul-esq-top" y={[-10, 50]} tagOuter="figure">
                                <img src={this.props.azulEsqTop} alt=""/>
                            </Parallax>
                        : null}
                        {this.props.azulDirTop ?
                            <Parallax className="bola azul-dir-top" y={[-30, 700]} tagOuter="figure">
                                <img src={this.props.azulDirTop} alt=""/> 
                            </Parallax>
                        : null}
                        {this.props.bolasCinza ?
                            <Parallax className="bolas-cinza" y={[-10, 10]} tagOuter="figure">
                                <img src={this.props.bolasCinza} alt=""/> 
                            </Parallax>
                        : null}
                        {this.props.imgCards ? <img className="cards" src={this.props.imgCards} alt=""/> : null}
                        <img className="principal" src={this.props.img} alt=""/>
                    </ImgContainer>
                </div>
            </section>
        )
    }
}
