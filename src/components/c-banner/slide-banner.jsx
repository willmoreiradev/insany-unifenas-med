import React, { Component } from 'react';
import Swiper from 'react-id-swiper';
import styled from 'styled-components';

import BtnAmarelo from './../btn-amarelo';
import BtnBranco from './../btn-branco';

const Slide = styled.div`
     width: 100%;
    height: 650px;
    background: url(${props => (props.bg)}) no-repeat center center;
`

const slideBanner = (props) => {
  const params = {
    navigation: {
      nextEl: '.s-banner-main .ctrl-slide .btn-next',
      prevEl: '.s-banner-main .ctrl-slide .btn-prev'
    },
    speed: 800,
    spaceBetween: 30
  }
  let slides = [];
  slides = props.slides;

  return (
    <Swiper {...params}>
      {
        slides.map((slide, i) => (
            slide ? (
            <Slide bg={slide.bg} key={i} >
              <div className="container">
                <div className="text-slide">
                  <h2>{slide.titulo}</h2>
                  <p>{slide.texto}</p>
                  {props.btn === 'amarelo' ? <BtnAmarelo link="http://inscricao.unifenas.br/index.aspx?v=VEST202MED" text="Quero fazer parte disso" /> : <BtnBranco link="http://inscricao.unifenas.br/index.aspx?v=VEST202MED" text="Quero ser MED Unifenas" />}
                </div>
                {slide.imagem ? <img loading="lazy" style={props.path == '/bh' ? {left: '25%'} : null} src={slide.imagem} alt="" /> : null }
              </div>
            </Slide>
            ) : null
        ))
      }
    </Swiper>
  )
}

export default slideBanner;