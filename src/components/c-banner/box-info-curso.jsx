import React, { Component } from 'react';

import Styled from 'styled-components';

import IconeCalendario from '../../images/icone-calendario.svg';
import IconePeriodo from '../../images/icone-periodo.svg';
import IconeCampus from '../../images/icone-campus.svg';

const StyledBox = Styled.div` 
    width: 237px;
    height: 267px;
    box-shadow: 0 16px 23px rgba(0, 0, 0, 0.07);
    border-radius: 6px;
    background-color: #ffffff;
    display: flex;
    align-items: center;
    padding-left: 46px;
    & ul {
        & li {
            display: flex;
            align-items: center;
            margin-bottom: 35px;
            &:last-child {
                margin-bottom: 0px;
            }
            & .icone {
                margin-right: 20px;
                width: 25px;
                display: flex;
                justify-content: center;
            }
            & span {
                display: block;
                color: #57555f;
                font: normal 400 16px/1 'Roboto';
                margin-bottom: 3px;
            }
            & strong {
                color: #57555f;
            }
        }
    }
    @media(max-width: 1050px) {
        width: 336px;
        height: 170px;
        padding: 0;
        & ul {
            display: flex;
            flex-direction: row;
            justify-content: space-evenly;
            flex-wrap: wrap;
            align-items: center;
            & li{
                padding: 10px;
                margin-bottom: 0;
            }
        }
    }
`

class BoxInfoCurso extends Component {
    render() {
        return (
            <StyledBox className="box-white">
                <ul>
                    <li>
                        <div className="icone">
                            <img src={IconeCalendario} alt="" />
                        </div>
                        <div className="info">
                            <span>Duração:</span>
                            <strong>{this.props.duracao}</strong>
                        </div>
                    </li>
                    <li>
                        <div className="icone">
                            <img src={IconePeriodo} alt="" />
                        </div>
                        <div className="info">
                            <span>Período:</span>
                            <strong>{this.props.periodo}</strong>
                        </div>
                    </li>
                    <li>
                        <div className="icone">
                            <img src={IconeCampus} alt="" />
                        </div>
                        <div className="info">
                            <span>Câmpus:</span>
                            <strong>{this.props.campus}</strong>
                        </div>
                    </li>
                </ul>
            </StyledBox>
        );
    }
}

export default BoxInfoCurso;