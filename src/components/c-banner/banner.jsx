import React, { Component } from 'react';

import Styled from 'styled-components';

import Slide from './slide-banner';

import BoxInfoCurso from '../c-banner/box-info-curso';

import LineTextBanner from '../../images/line-white.svg';
import BtnPrevSlide from '../../images/arrow-prev.svg';
import BtnNextSlide from '../../images/arrow-next.svg';

const ConteudoBanner = Styled.section`
    width: 100%;
    height: 650px;
    background: url(${props => (props.bg) }) no-repeat center center;
    transition: all .3s;
    margin-top: 80px;
    &:after {
        content: "";
        background: url(${props => (props.dots)}) no-repeat center center;
        position: absolute;
        top: 0;
        right: 0;
        width: 1260px;
        height: 100%;
        background-size: cover;
        pointer-events: none;
    }
`

class Banner extends Component {

        state = {
             activeSlide: 1
        }

    
    
    render() {
        return (
            <ConteudoBanner className="s-banner-main" bg={this.props.bg} bg1={this.props.bg1} bg2={this.props.bg2} dots={this.props.dots}  active={this.state.activeSlide}>
                <Slide
                    slides={this.props.slides}
                    btn={this.props.btn}
                    path={this.props.path}
                />
                <div className="area-info-curso">
                    <div className="container">
                        <div className="ctrl-slide">
                            <img src={BtnPrevSlide} className="btn btn-prev" onClick={()=>this.setState({activeSlide: 1})} alt=""/>   
                            <img src={BtnNextSlide} className="btn btn-next" onClick={()=>this.setState({activeSlide: 2})} alt=""/>  
                        </div>
                        <div className="text-info">
                            {this.props.children}
                            <img loading="lazy" src={LineTextBanner} alt="" className="line" />
                        </div>
                        <BoxInfoCurso duracao={this.props.infoCurso.duracao} periodo={this.props.infoCurso.periodo} campus={this.props.infoCurso.campus} />
                    </div>
                </div>
            </ConteudoBanner>
        );
    }
}

export default Banner;
