import React, { Component } from 'react'
import styled from 'styled-components'

const ImgContainer = styled.div`
    width: 100%;
    height: 229px;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    align-items: center;
    display: flex;
    align-items: center;
    justify-content: center;
    border-top-left-radius: 6px;
    border-top-right-radius: 6px;
    img{
        max-width: 500px;
    }
`

export default class CardNovidade extends Component {
    render() {
        return (
            <a href={this.props.link} target="_blank">
                <div className="card-novidade">
                    <ImgContainer>
                        <img src={this.props.img} alt=""/>
                    </ImgContainer>
                    <div className="texto">
                        <span className="data">
                            {this.props.data}
                        </span>
                        <h4 className="titulo">
                            {this.props.titulo}
                        </h4> 
                        <span className="ver-mais">
                            Ver mais
                        </span> 
                    </div>
                </div>
            </a>
        )
    }
}
