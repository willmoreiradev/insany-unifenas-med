import React, { Component } from 'react'

import ArrowPrev from '../../images/arrow-prev-depo.svg';
import ArrowNext from '../../images/arrow-next-depo.svg';
import SliderNovidades from './slider-novidades';

export default class Novidades extends Component {
    state={
        noticias: []
    }
    async componentWillMount() {
        const options = {
            method: 'GET',
            headers: new Headers({
                'Content-Type': ' application/json',
                'Authorization': 'Basic QzUxMTBDRkRCMzFGNDU5Nzg2Rjg2MDNBRDUzMTZGNzU6MUREOEU3MDJEQjRCNDEzRUFGRTU2NEUxN0UzM0M5RTY='
            })
        };
        await fetch(`https://intranet.unifenas.br/ApiNoticiaseEventos/v1/noticias`, options)
        .then(response => response.json()) // retorna uma promise
        .then(result => {   
            this.setState({noticias: result.Objeto.slice(0,8)});
        })
        .catch(err => {
            console.error('Failed retrieving information', err);
        });
    }
    
    render() {
        return (
            <section className="s-novidades">
                <div className="container">
                    <div className="text">
                        <div className="info">
                            <h1>Novidades</h1>
                            <p>Conheça o que há de novo na universidade e descubra por que é incrível #SerMaisUnifenas</p>
                        </div>
                        <div className="ctrl-slide-nov ctrl-slide">
                            <a href="https://www.unifenas.br/noticias.asp" target="_blank">Confira todas as novidades</a>
                            <img src={ArrowPrev} className="btn btn-prev" alt=""/>
                            <img src={ArrowNext} className="btn btn-next" alt=""/>
                        </div>
                    </div>
                    <SliderNovidades novidades={this.state.noticias} />
                </div>
            </section>
        )
    }
}
