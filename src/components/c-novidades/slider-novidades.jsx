import React from 'react';
import Swiper from 'react-id-swiper';
import CardNovidade from './card-novidade';

const SliderNovidades = (props) => {
    const params = {
        Swiper,
        navigation: {
            nextEl: '.ctrl-slide-nov .btn-next',
            prevEl: '.ctrl-slide-nov .btn-prev'
        },
        slidesPerView: 'auto',
        speed: 800,
        spaceBetween: 8,
        loop: true,
    }
    let novidades = []
    novidades = props.novidades
    
    return (
        <Swiper {...params} shouldSwiperUpdate>
            {
                novidades.map((novidade, i) => (
                    novidade ? (
                        <div key={i}>
                            <CardNovidade
                                link={`https://www.unifenas.br/noticia.asp?note=uni_${novidade.Codigo}`}
                                img={novidade.foto}
                                data={novidade.data_cadastro.slice(0,10)}
                                titulo={`${novidade.Titulo.slice(0,38)} ...`}
                            />
                        </div>
                    ) : null
                ))
                
            }
        </Swiper>
    )
}

export default SliderNovidades;