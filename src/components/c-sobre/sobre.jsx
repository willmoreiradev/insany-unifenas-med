import React, { Component } from 'react';

import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import ReactPlayer from 'react-player'

import "react-tabs/style/react-tabs.css";

import SlidePeriodoBh from './slide-periodos-bh';
import SlidePeriodoAlfenas from './slide-periodos-alfenas';
import BoxCoordenacao from './c-box-coordenacao';

import SliderFotosLab from './slide-fotos-lab';

import ArrowPrev from '../../images/seta-matriz-esq.svg';
import ArrowNext from '../../images/seta-matriz-dir.svg';

import ArrowPrevS from '../../images/arrow-prev-depo.svg';
import ArrowNextS from '../../images/arrow-next-depo.svg';

import ThumbVideo from '../../images/thumb-video.png';
import IconeEnvelope from '../../images/icone-envelope.svg';
import IconePhone from '../../images/icone-phone.svg';
import Prouni from '../../images/logo-prouni.png';
import BradescoUniversitario from '../../images/logo-universitario.png';
import NovoFies from '../../images/logo-novo-fies.png';
import Santander from '../../images/logo-santander.png';
import Pravaler from '../../images/logo-pravaler.png';
import IconeCash from '../../images/icone-cash.svg';
import fotoCoordenacao from '../../images/foto-coord-01.jpg';

import IconeCalendario from '../../images/icone-calendario.svg';
import IconePeriodo from '../../images/icone-periodo.svg';


class PeriodoTable extends Component{
    render(){
        if(this.props.cidade == "alfenas"){
            if(this.props.activePeriodo == 1){
                return(
                    <table border={1}>
                        <thead>
                            <tr>
                                <th>Eixo</th>
                                <th>Módulo</th>
                                <th>Disciplinas</th>
                                <th>Carga Horária</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td rowSpan={6}>
                                    Fundamentação técnico - científica
                                </td>
                                <td rowSpan={5}>
                                    Bioestrutural
                                </td>
                                <td>
                                    Anatomia I
                                </td>
                                <td>
                                    100
                                </td>
                            </tr>
                            <tr>
                                <td>Bioquímica Médica I</td>
                                <td>67</td>
                            </tr>
                            <tr>
                                <td>Bases Moleculares e Celulares</td>
                                <td>67</td>
                            </tr>
                            <tr>
                                <td>Genética Geral</td>
                                <td>33</td>
                            </tr>
                            <tr>
                                <td>Embriologia</td>
                                <td>67</td>
                            </tr>
                            <tr>
                                <td>Metodologia Científica I</td>
                                <td>Metodologia Científica I </td>
                                <td>33</td>
                            </tr>
                            <tr>
                                <td rowSpan={2}>Atenção Integral ao paciente e comunidade</td>
                                <td rowSpan={2}>Práticas de Saúde I</td>
                                <td>Políticas e Práticas de Saúde na Comunidade </td>
                                <td>133</td>
                            </tr>
                            <tr>
                                <td>Urgência e Emergência I</td>
                                <td>17</td>
                            </tr>
                            <tr>
                                <td>Formação Humanista</td>
                                <td>Ética e Espiritualidade </td>
                                <td>Ética e Espiritualidade</td>
                                <td>17</td>
                            </tr>
                            <tr>
                                <td>Integração Básico-Clínico</td>
                                <td>Casos Clínicos Colaborativos I </td>
                                <td>Casos Clínicos Colaborativos I</td>
                                <td>17</td>
                            </tr>
                        </tbody>
                    </table>
                )
            }else if(this.props.activePeriodo == 2){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={5}>
                                Fundamentação técnico - científica
                            </td>
                            <td rowSpan={5}>
                                Morfofuncional I
                            </td>
                            <td>
                                Anatomia II
                            </td>
                            <td>
                                100
                            </td>
                        </tr>
                        <tr>
                            <td>Neuroanatomia Funcional I</td>
                            <td>50</td>
                        </tr>
                        <tr>
                            <td>Bioquímica Médica II</td>
                            <td>67</td>
                        </tr>
                        <tr>
                            <td>Histologia Geral I</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>Fisiologia I</td>
                            <td>83</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}>Atenção Integral ao paciente e comunidade</td>
                            <td rowSpan={2}>Práticas de Saúde II</td>
                            <td>Vigilância e Educação em Saúde </td>
                            <td>67</td>
                        </tr>
                        <tr>
                            <td>Urgência e Emergência II</td>
                            <td>17</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>História da Medicina</td>
                            <td>História da Medicina</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos II </td>
                            <td>Casos Clínicos Colaborativos II</td>
                            <td>17</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>
                                Disciplina Optativa
                            </td>
                            <td>
                                33
                            </td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 3){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={6}>
                                Fundamentação técnico - científica
                            </td>
                            <td rowSpan={5}>
                                Morfofuncional II
                            </td>
                            <td>
                                Anatomia III 
                            </td>
                            <td>
                                100
                            </td>
                        </tr>
                        <tr>
                            <td>Histologia Geral II</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>Fisiologia II </td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>Farmacologia I</td>
                            <td>67</td>
                        </tr>
                        <tr>
                            <td>Neuroanatomia Funcional II</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>Metodologia Científica II</td>
                            <td>Metodologia Científica II </td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}>Atenção Integral ao paciente e comunidade</td>
                            <td rowSpan={2}>Práticas de Saúde III</td>
                            <td>Saúde Materno Infantil na Atenção Primária</td>
                            <td>133</td>
                        </tr>
                        <tr>
                            <td>Urgência e Emergência III</td>
                            <td>17</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>Ética do Início da vida</td>
                            <td>Psicologia Médica</td>
                            <td>17</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos III</td>
                            <td>Casos Clínicos Colaborativos III</td>
                            <td>17</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 4){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={5}>
                                Fundamentação técnico - científica
                            </td>
                            <td rowSpan={5}>
                                Biointeração
                            </td>
                            <td>
                                Farmacologia II  
                            </td>
                            <td>
                                83
                            </td>
                        </tr>
                        <tr>
                            <td>Imunologia</td>
                            <td>83</td>
                        </tr>
                        <tr>
                            <td>Microbiologia</td>
                            <td>83</td>
                        </tr>
                        <tr>
                            <td>Genética Médica</td>
                            <td>50</td>
                        </tr>
                        <tr>
                            <td>Parasitologia Médica </td>
                            <td>83</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}>Atenção Integral ao paciente e comunidade</td>
                            <td rowSpan={2}>Práticas de Saúde IV</td>
                            <td>Anamnese Clínica e Psicossocial </td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>Urgência e Emergência IV</td>
                            <td>17</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>Antropologia Médica</td>
                            <td>Antropologia Médica</td>
                            <td>17</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos IV</td>
                            <td>Casos Clínicos Colaborativos IV</td>
                            <td>17</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 5){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={4}>
                                Fundamentação técnico - científica
                            </td>
                            <td rowSpan={2}>
                                Diagnóstico Médico I
                            </td>
                            <td>
                                Histopatologia I 
                            </td>
                            <td>
                                100
                            </td>
                        </tr>
                        <tr>
                            <td>Análises Clínicas</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>
                                Gestão em Serviços de Saúde 
                            </td>
                            <td>
                                Gestão em Serviços de Saúde 
                            </td>
                            <td>
                                33
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Prática Hospitalar e Emergencial
                            </td>
                            <td>
                                Prática Hospitalar e Emergencial
                            </td>
                            <td>
                                33
                            </td>
                        </tr>
                        <tr>
                            <td rowSpan={2}>Atenção Integral ao paciente e comunidade</td>
                            <td>Práticas de Saúde V</td>
                            <td>Semiologia Médica I</td>
                            <td>167</td>
                        </tr>
                        <tr>
                            <td>Assistência Médica I</td>
                            <td>Assistência Preventiva e Terapêutica I</td>
                            <td>67</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>Deontologia e Medicina Legal</td>
                            <td>Deontologia e Medicina Legal</td>
                            <td>67</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos VI</td>
                            <td>Casos Clínicos Colaborativos VI</td>
                            <td>17</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 6){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={3}>
                                Fundamentação técnico - científica
                            </td>
                            <td rowSpan={2}>
                                Diagnóstico Médico II
                            </td>
                            <td>
                                Histopatologia II
                            </td>
                            <td>
                                100
                            </td>
                        </tr>
                        <tr>
                            <td>Diagnóstico por Imagem</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>Bases da Técnica Cirúrgica</td>
                            <td>Bases da Técnica Cirúrgica e Procedimentos Emergenciais</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td rowSpan={3}>Atenção Integral ao paciente e comunidade</td>
                            <td>Práticas de Saúde VI</td>
                            <td>Semiologia Médica II</td>
                            <td>167</td>
                        </tr>
                        <tr>
                            <td rowSpan={2}>Assistência Médica II</td>
                            <td>Nutrição Médica</td>
                            <td>50</td>
                        </tr>
                        <tr>
                            <td>Assistência Preventiva e Terapêutica II</td>
                            <td>83</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>Princípios da Bioética</td>
                            <td>Princípios da Bioética</td>
                            <td>33</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos VI</td>
                            <td>Casos Clínicos Colaborativos VI</td>
                            <td>17</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 7){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Fundamentação técnico - científica
                            </td>
                            <td>
                                Epidemiologia e Medicina Baseada em Evidências
                            </td>
                            <td>
                                Epidemiologia e Medicina Baseada em Evidências
                            </td>
                            <td>
                                21
                            </td>
                        </tr>
                        <tr>
                            <td rowSpan={3}>Atenção Integral ao paciente e comunidade</td>
                            <td>Saúde do Adulto I</td>
                            <td>Saúde do Adulto I</td>
                            <td>245</td>
                        </tr>
                        <tr>
                            <td>Saúde da Mulher</td>
                            <td>Saúde da Mulher</td>
                            <td>210</td>
                        </tr>
                        <tr>
                            <td>Clínica Cirúrgica </td>
                            <td>Cirurgia Ambulatorial e Hospitalar</td>
                            <td>245</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>Bioética Clínica</td>
                            <td>Bioética Clínica</td>
                            <td>21</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos VII</td>
                            <td>Casos Clínicos Colaborativos VII</td>
                            <td>17</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 8){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th>Eixo</th>
                            <th>Módulo</th>
                            <th>Disciplinas</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                Fundamentação técnico - científica
                            </td>
                            <td>
                                Metodologia Científica III
                            </td>
                            <td>
                                Metodologia Científica III
                            </td>
                            <td>
                                21
                            </td>
                        </tr>
                        <tr>
                            <td rowSpan={5}>Atenção Integral ao paciente e comunidade</td>
                            <td>Saúde do Adulto II</td>
                            <td>Saúde do Adulto II</td>
                            <td>245</td>
                        </tr>
                        <tr>
                            <td>Saúde da Criança e Adolescente</td>
                            <td>Saúde da Criança e Adolescente</td>
                            <td>210</td>
                        </tr>
                        <tr>
                            <td rowSpan={3}>Saúde da Família</td>
                            <td>Saúde da Família</td>
                            <td>126</td>
                        </tr>
                        <tr>
                            <td>Saúde do Trabalhador</td>
                            <td>35</td>
                        </tr>
                        <tr>
                            <td>Saúde Mental</td>
                            <td>84</td>
                        </tr>
                        <tr>
                            <td>Formação Humanista</td>
                            <td>Terminalidade</td>
                            <td>Cuidados Paliativos e Tanatologia</td>
                            <td>21</td>
                        </tr>
                        <tr>
                            <td>Integração Básico-Clínico</td>
                            <td>Casos Clínicos Colaborativos VII</td>
                            <td>Casos Clínicos Colaborativos VII</td>
                            <td>21</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 9){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Estágios</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={3}>
                                Estágio (Regime de Internato)
                            </td>
                            <td>
                                Estágio em Clínica Cirurgica I 
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estágio em Urgência e Emergência I
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estágio em Medicina Geral da Família e Comunidade I
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 10){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Estágios</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={3}>
                                Estágio (Regime de Internato)
                            </td>
                            <td>
                                Estágio em Pediatria e Puericultura I
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estágio em Clínica Médica I 
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Estágio em Ginecologia e Obstetrícia I
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 11){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Estágios</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={3}>
                                Estágio (Regime de Internato)
                            </td>
                            <td>
                                Estágio em Pediatria e Puericultura II 
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estágio em Clínica Médica II
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estágio em Clínica Cirurgica II 
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 12){
                return(
                    <table border={1}>
                    <thead>
                        <tr>
                            <th></th>
                            <th>Estágios</th>
                            <th>Carga Horária</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td rowSpan={3}>
                                Estágio (Regime de Internato)
                            </td>
                            <td>
                                Estágio em Ginecologia e Obstetrícia II 
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estágio em Urgência e Emergência II
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                        <tr>
                            <td>
                            Estágio em Medicina Geral da Família e Comunidade II 
                            </td>
                            <td>
                                280
                            </td>
                        </tr>
                    </tbody>
                </table>
                )
            }else{
                return null
            }
        }else if (this.props.cidade == "bh"){
            if(this.props.activePeriodo == 1){
                return(
                    <table border={1} className="table-bh">
                        <thead>
                            <tr>
                                <th colSpan={4}>Sistemas regulatórios ciclos de vida</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr className="periodo-row">
                                <td>1º período</td>
                                <td>2º período</td>
                                <td>3º período</td>
                                <td>4º período</td>
                            </tr>
                            <tr>
                                <td>Homeostasia</td>
                                <td>Epidemia</td>
                                <td>Células e moléculas</td>
                                <td>Puberdade e adolescência</td>
                            </tr>
                            <tr>
                                <td>Hemorragia e choque</td>
                                <td>Insconsciência</td>
                                <td>Nutrição e metabolismo</td>
                                <td>Vida adulta</td>
                            </tr>
                            <tr>
                                <td>Oligúria</td>
                                <td>Absome agudo</td>
                                <td>Gestação</td>
                                <td>Meia idade</td>
                            </tr>
                            <tr>
                                <td>Dispneia</td>
                                <td>Febre</td>
                                <td>Nascimento e crescimento</td>
                                <td>Idoso</td>
                            </tr>
                        </tbody>
                    </table>
                )
            }else if(this.props.activePeriodo == 2){
                return(
                    <table border={1} className="table-bh">
                    <thead>
                        <tr>
                            <th colSpan={4}>Síndromes clínicas e cirúrgicas</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="periodo-row">
                            <td>5º período</td>
                            <td>6º período</td>
                            <td>7º período</td>
                            <td>8º período</td>
                        </tr>
                        <tr>
                            <td>Síndrome pediátricas II</td>
                            <td>Síndrome pediátricas II</td>
                            <td>Síndrome ginecológicas</td>
                            <td>Urgências e emergências</td>
                        </tr>
                        <tr>
                            <td>Síndrome cardiológicas</td>
                            <td>Síndrome nefrourológicas</td>
                            <td>Síndrome neuropsiquiátricas</td>
                            <td>Síndrome reumatológicas e ortopédicas</td>
                        </tr>
                        <tr>
                            <td>Síndrome respiratórias</td>
                            <td>Síndrome oncohematológicas</td>
                            <td>Síndrome endocrinológicas</td>
                            <td>Síndrome obstétricas</td>
                        </tr>
                        <tr>
                            <td>Síndrome digestórias</td>
                            <td>Síndrome infecciosas</td>
                            <td>Síndrome dermatológicas</td>
                            <td>Síndromes cirúrgicas</td>
                        </tr>
                    </tbody>
                </table>
                )
            }else if(this.props.activePeriodo == 3){
                return(
                    <table border={1} className="table-bh">
                    <thead>
                        <tr>
                            <th colSpan={4}>Estágio curricular (em regime de internato)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="periodo-row">
                            <td>9º período</td>
                            <td>10º período</td>
                            <td>11º período</td>
                            <td>12º período</td>
                        </tr>
                        <tr>
                            <td>Clínica médica</td>
                            <td>Saúde da mulher</td>
                            <td>Atenção integral à saúde I</td>
                            <td>Urg e emergências I e saúde mental</td>
                        </tr>
                        <tr>
                            <td>Clínica cirúrgica</td>
                            <td>Saúde da criança</td>
                            <td>Atenção integral à saúde II</td>
                            <td>Urgências e emergências II</td>
                        </tr>
                    </tbody>
                </table>
                )
            }
        }
    }
}

class SectionSobre extends Component {
    state={
        activePeriodo: 1
    }
    myCallback = (dataFromChild) => {
        this.setState({activePeriodo: dataFromChild})
    }
    render() {
        return (
            <section className="s-sobre">
                <Tabs>
                    <div className="navegacao">
                        <div className="container">
                            <TabList>
                                <Tab>Sobre o Curso</Tab>
                                <Tab>Investimento</Tab>
                                {/* <Tab>Matriz curricular</Tab> */}
                                <Tab>Coordenação</Tab>
                                <Tab>Laboratórios</Tab>
                            </TabList>
                        </div>
                    </div>

                    <div className="conteudo">
                        <div className="container">
                            <TabPanel>
                                <div className="sobre-curso">
                                    <div className="texto">
                                        <h1>Sobre o Curso</h1>
                                        <span className="texto-i">Eleito o melhor curso privado de Minas Gerais</span>
                                        <p>
                                            Desde o 1º período, o curso de Medicina da UNIFENAS insere o futuro médico nas práticas na comunidade. A partir do 7º período, até o 12º, em sistema modular, privilegia a prática médica e a reflexão. Do 9º ao 12º períodos, oferece o internato médico, com plantão em seis grandes áreas: Clínica Médica, Pediatria, Clínica Cirúrgica, Ginecologia e Obstetrícia, Saúde da Família e Comunidade e Urgência e emergência.
                                        </p>
                                        
                                    </div>
                                    <div className="video">
                                        <ReactPlayer url={this.props.videoLink} light playing />
                                        <span>{this.props.videoDescricao}</span>
                                    </div>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="investimento">
                                    <div className="texto">
                                        {this.props.path === '/bh' || this.props.path === '/bh/' ? <h2>Mensalidade Medicina Belo Horizonte</h2> : <h2>Mensalidade Medicina Alfenas</h2> }
                                        <strong>Setor de Financiamentos e Prouni</strong>
                                        <p>
                                            O Setor de Financiamentos e Prouni tem como objetivo primordial buscar alternativas que auxiliem os alunos a suprir dificuldades sociais que possam interferir em sua permanência na UNIFENAS.
                                        </p>
                                        <ul className="contato">
                                            <li>
                                                <img src={IconeEnvelope} alt="" />
                                                {this.props.path === '/bh' || this.props.path === '/bh/' ? <span>medicina.bh@unifenas.br</span> : <span>medicina.alfenas@unifenas.br</span> }
                                            </li>
                                            <li>
                                                <img src={IconePhone} alt="" />
                                                {this.props.path === '/bh' || this.props.path === '/bh/' ? <span>0800 283 6060  |  (31) 2536-5681</span> : <span>0800 283 6060  |  (35) 3299-3166</span> }
                                            </li>
                                        </ul>
                                        <div className="bolsas">
                                            <h3>Bolsas e financiamentos</h3>
                                            <ul>
                                                <li><img src={Prouni} alt="" /></li>
                                                <li><img src={BradescoUniversitario} alt="" /></li>
                                                <li><img src={NovoFies} alt="" /></li>
                                                <li><img src={Santander} alt="" /></li>
                                                <li><img src={Pravaler} alt="" /></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div className="box-ingressante">
                                        <div className="topo">
                                            <h3>Ingressantes 2020</h3>
                                        </div>
                                        <div className="info">
                                            <img src={IconeCash} alt="" />
                                            <span>Medicina Humana</span>
                                            <h4><span>R$</span> {this.props.preco}</h4>
                                        </div>
                                    </div>
                                </div>
                            </TabPanel>
                            {/* <TabPanel>
                                <div className="matriz-curricular">
                                    <div className="ctrl-slide-matriz ctrl-slide">
                                        <img src={ArrowPrev} className="btn btn-prev" id="seta-slide-matriz-esq" alt=""/>
                                        {this.props.path === '/bh' || this.props.path === '/bh/' ?  <SlidePeriodoBh periodos={this.props.periodos} activePeriodo={this.state.activePeriodo} callbackFromParent={this.myCallback} /> :  <SlidePeriodoAlfenas periodos={this.props.periodos} activePeriodo={this.state.activePeriodo} callbackFromParent={this.myCallback} /> }          
                                        <img src={ArrowNext} className="btn btn-next" id="seta-slide-matriz-dir" alt=""/>
                                    </div>
                                    
                                    <PeriodoTable cidade={this.props.cidade} activePeriodo={this.state.activePeriodo} />
                                </div>
                            </TabPanel> */}
                            <TabPanel>
                                <div className="container">
                                    <div className="coordenacao">
                                    {
                                        this.props.coordenacao.map((item, i) => (
                                            item ? (
                                                <BoxCoordenacao key={i} foto={item.imagem} funcao={item.funcao} nome={item.nome} />
                                            ) : null
                                        ))
                                    }
                                    </div>
                                </div>
                            </TabPanel>
                            <TabPanel>
                                <div className="laboratorios">
                                    <div className="texto">
                                        <h1>Laboratórios</h1>
                                        <p>
                                            {this.props.textoLab}
                                        </p>
                                    </div>
                                    <SliderFotosLab
                                        slidesLab={this.props.slidesLab}
                                    />
                                    <div className="ctrl-slide-lab ctrl-slide">
                                        <img src={ArrowPrevS} className="btn btn-prev" alt=""/>
                                        <img src={ArrowNextS} className="btn btn-next" alt=""/>
                                    </div>
                                </div>
                            </TabPanel>
                        </div>
                    </div>
                </Tabs>
            </section>
        );
    }
}

export default SectionSobre;