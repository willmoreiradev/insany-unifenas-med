import React from 'react';
import Swiper from 'react-id-swiper';

const SliderFotosLab = (props) => {
    const params = {
        navigation: {
            nextEl: '.ctrl-slide-lab .btn-next',
            prevEl: '.ctrl-slide-lab .btn-prev'
        },
        slidesPerView: 'auto',
        speed: 800,
        spaceBetween: 0,
        
    }

    let slidesLab = []
    slidesLab = props.slidesLab

    return (
        <Swiper {...params}>
            {
            slidesLab.map((item, i) => (
                item ? (
                <div key={i}>
                    <div className="foto-hospital">
                        <img src={item.imagem} />
                        <span>{item.descricao}</span>
                    </div>
                </div>
                ) : null
            ))
            }
        </Swiper>
    )
}

export default SliderFotosLab;