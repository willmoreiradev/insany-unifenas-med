import React, { Component } from 'react';

class BoxCoordenacao extends Component {
    render() { 
        return ( 
            <div className="box-coordenacao">
                <div className="foto">
                    <img src={this.props.foto} alt=""/>
                </div>
                <div className="info">
                    <h3>{this.props.nome}</h3>
                    <span>{this.props.funcao}</span>
                </div>
            </div>
        );
    }
}
 
export default BoxCoordenacao;