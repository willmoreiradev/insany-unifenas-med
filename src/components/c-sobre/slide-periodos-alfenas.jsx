import React, {Component} from "react"

import Swiper from 'react-id-swiper';


class slidePeriodo extends Component {
  state = {
    active: this.props.activePeriodo
  }
  componentDidMount(){
    let per = document.querySelectorAll(".periodo");
    per[0].classList.add("active-periodo");
  }
  someFn = (e) => {
    this.props.callbackFromParent(e.target.getAttribute("data-index"));
    let p = document.querySelectorAll(".periodo");
    p.forEach((pe) =>{
      pe.classList.remove("active-periodo");
    });
    e.target.classList.add("active-periodo");
  }
  render(){

    const params = {
      navigation: {
        nextEl: '#seta-slide-matriz-dir',
        prevEl: '#seta-slide-matriz-esq'
      },
      slidesPerView: 'auto',
      speed: 800,
      containerClass: 'swiper-container slider-p-al'

    }

    let periodos = [];
    periodos = this.props.periodos;

    return (
      <Swiper {...params}>
        {
        periodos.map((periodo, i) => (
            periodo ? (
            <div data-index={i+1} key={i} className="periodo" onClick={this.someFn}>
              {periodo}
            </div>
            ) : null
        ))
      }
      </Swiper>
    )
  }
}

export default slidePeriodo;