import React, { Component } from 'react'

import BtnAmarelo from '../btn-amarelo';
export default class ActionCard extends Component {
    handleClick = () => {
        document.documentElement.classList.toggle("hide-card")
    }   
    render() {
        return (
            <div className={this.props.isHorarios ? "card cardH" : "card"}>
                <div className="card-container">
                    <div className="icon">
                      <img src={this.props.icon} />
                        <h1>{this.props.title}</h1>
                    </div>
                    <p>{this.props.text}</p>
                    {this.props.children}
                    <div onClick={this.handleClick} >
                        <BtnAmarelo disabled={this.props.disabled} text={this.props.btnText} isHorarios={this.props.isHorarios} />
                    </div>
                </div>
            </div>
        )
    }
}
