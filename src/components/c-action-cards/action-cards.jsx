import React, { Component } from 'react'
import styled from 'styled-components'
import ActionCard from './card'

import file from '../../images/file.svg';
import talk from '../../images/talk.svg';
import clock from '../../images/clock.svg';

import FechaIcon from '../../images/close-modal.svg'

const InfoContainer = styled.div`
    position: relative;
    display: none;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    pointer-events: none;
    transition: all .3s;
    .fechaInfoCardH{
        display: none;
        pointer-events: none;
        position: absolute;
        right: -10px;
        top: -36px;
        cursor: pointer;
        transition: all .3s;

        @media (max-width: 1050px){
            right: 0px;
            top: -20px;
        }
    }
    h3{
        margin-bottom: 24px;
        color: #0f538a;
        font-family: "DINPro";
        font-size: 18px;
        font-weight: 700;
    }
    ul {
        height: 350px;
        overflow-y: auto;
        padding-right: 80px;
    }
    ul::-webkit-scrollbar-track {
        border-radius: 10px;
        background-color: #f8f8f86e;
    }
    ul::-webkit-scrollbar {
        width: 5px;
        background-color: #f8f8f86e;
    }
    ul::-webkit-scrollbar-thumb {
        border-radius: 10px;
        background-color: #0f538a;
    }
    li{
        margin-bottom: 20px;
        a{
            line-height: 22px;
            color: #00abc5;
            font-size: 16px;
            font-weight: 400;
        }   
    }

`

export default class ActionCards extends Component {
    handleClick = () => {
        document.documentElement.classList.toggle("hide-card")
    }   
    render() {
        return (
            <section className="s-action-cards">
                <div className="container">
                    <ActionCard 
                        icon={file}
                        title="Guia do calouro"
                        text="Montamos um guia com informações que todo calouro precisa saber, como nome e telefone dos principais restaurantes, hospitais, farmácias, hoteis, padarias, supermercados da cidade."
                        btnText="Acesse o Guia do Calouro"
                        disabled
                    />
                    <ActionCard
                        icon={talk}
                        title="Teste interativo"
                        text="Qual área da med mais combina com você? Responda o quiz e veja qual área pode ser a que mais combina com você."
                        btnText="Responda o quiz"
                        disabled
                    />
                    <ActionCard
                        icon={clock}
                        isHorarios={true}
                        title="Horários"
                        text="Sabemos que para evoluir é preciso se organizar. Pensando nisso preparamos uma tabelinha de planejamento de horários para facilitar sua vida ;)"
                        btnText="Veja o planejamento"
                        disabled={false}
                    >
                        <InfoContainer className="InfoCardH">
                            <img className="fechaInfoCardH" src={FechaIcon} onClick={this.handleClick} />
                            <h3>Selecione:</h3>
                            
                            {this.props.path == '/bh' || this.props.path == '/bh/' ? 
                                <ul>
                                   {/* <li>
                                       <a href="http://www.unifenas.br/portaldoaluno/Cronograma%20Discente%202019_2.pdf" target="_blank">
                                           Cronograma Discente 2019/2
                                       </a>
                                   </li> */}
                                   <li>
                                       <a href="http://www.unifenas.br/portaldoaluno/medicinabh/20201/BH_1-8.jpg" target="_blank">
                                           Calendário Acadêmico 2020 <br />1º ao 8º periodos
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/portaldoaluno/medicinabh/20201/BH_9e10.jpg" target="_blank">
                                            Calendário Acadêmico 2020 <br />9º ao 10º periodos 
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/portaldoaluno/medicinabh/20201/BH_11-12.jpg" target="_blank">
                                            Calendário Acadêmico 2020 <br />11º ao 12º periodos
                                       </a>
                                   </li>
                                   {/* <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%201%C2%BA%20PER%C3%8DODO%202019.2.pdf" target="_blank">
                                            Horário 1º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%202%C2%BA%20PER%C3%8DODO%202019.2.pdf" target="_blank">
                                            Horário 2º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%203%C2%BA%20PER%C3%8DODO%20-%202019.2.pdf" target="_blank">
                                            Horário 3º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%204%C2%BA%20PER%C3%8DODO%20-%20%202019.2%20.pdf" target="_blank">
                                            Horário 4º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%205%C2%BA%20PER%C3%8DODO%20-%202019.2.pdf" target="_blank">
                                            Horário 5º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%206%C2%BA%20PER%C3%8DODO%20-%202019.2.pdf" target="_blank">
                                            Horário 6º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%207%C2%BA%20PER%C3%8DODO%20-%202019.2.pdf" target="_blank">
                                            Horário 7º Período <br />2° Semestre/2019.
                                       </a>
                                   </li>
                                   <li>
                                       <a href="http://www.unifenas.br/calendario/2019/2semestre/Hor%C3%A1rio%208%C2%BA%20PER%C3%8DODO%20%202019.2.pdf" target="_blank">
                                            Horário 8º Período <br />2° Semestre/2019.
                                       </a>
                                   </li> */}
                               </ul>
                            : 
                                <ul>
                                    <li>
                                        <a href="http://www.unifenas.br/calendario/2020/1semestre/calendario-2020-MEDAlfenas.jpg" target="_blank">
                                            Calendário Acadêmico 2020 - Alfenas
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.unifenas.br/calendario/2020/1semestre/calendario-2020-MED_passos.jpg" target="_blank">
                                            Calendário Acadêmico 2020 - Passos
                                        </a>
                                    </li>
                                    <li>
                                        <a href="http://www.unifenas.br/calendario/2020/1semestre/calendario-2020-MED_Varginha.jpg" target="_blank">
                                            Calendário Acadêmico 2020 - Varginha
                                        </a>
                                    </li>
                                </ul>
                            }
                        </InfoContainer>
                    </ActionCard>
                </div>
            </section>
        )
    }
}
