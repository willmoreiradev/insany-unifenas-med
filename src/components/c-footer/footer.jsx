import React, { Component } from 'react'

import SetaTopo from '../../images/seta-topo.svg';
import LogoWhite from '../../images/logo-unifenas-footer.svg'
import IconeInscrever from './../../images/icone-inscrever.svg';
import IconeAluno from './../../images/icone-aluno.svg';
import LogoInsany from '../../images/logo-insany.svg'

export default class Footer extends Component {
    state = {
        intervalId: 0,
        thePosition: false
    }; 
    componentDidMount() { 
        document.addEventListener("scroll", () => { 
            if (window.scrollY > 170) { 
                this.setState({ thePosition: true }) 
            } else { 
                this.setState({ thePosition: false }) 
            } 
        }); 
        window.scrollTo(0, 0); 
        } 
    onScrollStep = () => { 
        if (window.pageYOffset === 0){ 
            clearInterval(this.state.intervalId); 
        } 
        window.scroll(0, window.pageYOffset - 100); 
    } 
    scrollToTop = () => { 
        let intervalId = setInterval(this.onScrollStep, 30); 
        this.setState({ intervalId: intervalId }); 
    } 
    render() {   
        return (
            <footer className="s-footer">
                <div className="container">
                    <div className="topo">
                        <a className="btn-box" onClick={this.scrollToTop}>
                            <img src={SetaTopo} alt=""/>
                        </a>
                    </div>
                    <div className="info">
                        <div className="esq">
                            <img src={LogoWhite} alt=""/>
                            <span>© UNIFENAS - Universidade José do Rosário Vellano. Todos os direitos reservados.</span>
                        </div>
                        <div className="dir">
                            <ul className="btns">
                                <li>
                                    <a href="http://inscricao.unifenas.br/index.aspx?v=VEST202MED" target="_blank">
                                    <img src={IconeInscrever} alt="" />
                                    <span>Inscreva-se</span>
                                    </a>
                                </li>
                                {/* <li>
                                    <a href={this.props.path == '/bh' || this.props.path == '/bh/' ? 'http://www.unifenas.br/portalmedicinabh.asp' : 'http://www.unifenas.br/portalmedicinaalfenas.asp'} target="_blank">
                                    <img src={IconeAluno} alt="" />
                                    <span>Portal do aluno</span>
                                    </a>
                                </li> */}
                            </ul>
                            <a href="https://insanydesign.com" className="insany" target="_blank">
                                <span>Feito por:</span>
                                <img src={LogoInsany} alt=""/>
                            </a>    
                        </div> 
                    </div>
                </div>
            </footer>
        )
    }
}
