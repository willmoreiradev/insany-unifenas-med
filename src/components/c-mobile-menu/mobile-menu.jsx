import React, { Component } from 'react'
import { Link } from "gatsby"

import Logo from './../../images/logo.svg';
import IconePin from './../../images/pin-cidade.svg';
import ArrowSelect from './../../images/arrow-select.svg';
import IconeWhatsApp from './../../images/icone-whatsapp.svg';
import IconeInscrever from './../../images/icone-inscrever.svg';
import IconeAluno from './../../images/icone-aluno.svg';

export default class MobileMenu extends Component {
    showDropdown = (e) => {
        let elem = e.currentTarget.parentElement;
        elem.classList.toggle('show-cidade');
    }
    activeMenu = (e) => {
        e.preventDefault();
        document.documentElement.classList.toggle('menu-opened');
    }
    render() {
        return (
            <div className="mobile-menu">
                <nav>
                    <div className="cidade">
                    <div className="item-selected" onClick={this.showDropdown}>
                        <img src={IconePin} className="icone" />
                        {this.props.path == '/bh' || this.props.path == '/bh/' ? <span>Medicina <strong>Belo Horizonte</strong></span> : <span>Medicina <strong>Alfenas</strong></span>}
                        <img src={ArrowSelect} className="arrow" />
                    </div>
                    <ul className="dropdown">
                        <li onClick={this.activeMenu}>
                            <Link to="/alfenas" replace >Med. Alfenas</Link>
                        </li>
                        <li onClick={this.activeMenu}>
                            <Link to="/bh" replace >Med. Belo Horizonte</Link>
                        </li>
                    </ul>
                    </div>
                    <a href={this.props.whatsapp} target="_blank" className="whats-app">
                    <img src={IconeWhatsApp} alt="" />
                    <span>Whatsapp da Med</span>
                    </a>
                    <ul className="btns">
                    <li>
                        <a href="http://vestibular.unifenas.br/">
                        <img src={IconeInscrever} alt="" />
                        <span>Inscreva-se</span>
                        </a>
                    </li>
                    <li>
                        <a href="https://tiu.unifenas.br/">
                        <img src={IconeAluno} alt="" />
                        <span>Portal do aluno</span>
                        </a>
                    </li>
                    </ul>
                </nav>
            </div>
        )
    }
}
