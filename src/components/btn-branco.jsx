import React, { Component } from 'react';

import styled from 'styled-components';

const StyledBtn = styled.a `
    width: 288px;
    height: 51px;
    border-radius: 6px;
    background-color: #ffffff;
    display: flex;
    justify-content: center;
    align-items: center;
    color: #807e86;
    font-family: "DINPro";
    font-size: 16px;
    font-weight: 700;
    transform: scale(1);
    transition: all .3s;
    &:hover {
        transform: scale(1.05);
        transition: all .3s;
    }
`


class BtnBranco extends Component {
    render() { 
        return ( 
            <StyledBtn href={this.props.link} target="_blank">{this.props.text}</StyledBtn>
        );
    }
}
 
export default BtnBranco;