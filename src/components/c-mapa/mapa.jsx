import React, { Component } from 'react'

export default class Mapa extends Component {
    render() {
        return (
            <section className="s-mapa-contato" id="mapa">
                <iframe scrolling="no" src={this.props.mapa}></iframe>
            </section>
        )
    }
}
