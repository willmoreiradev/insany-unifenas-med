import React from 'react';
import Swiper from 'react-id-swiper';
import CardAgenda from './card-agenda';



const SliderAgenda = (props) => {
    const params = {
        navigation: {
            nextEl: '.ctrl-slide-ag .btn-next',
            prevEl: '.ctrl-slide-ag .btn-prev'
        },
        slidesPerView: 'auto',
        speed: 800,
        spaceBetween: 18,
        loop: true
    }

    let agenda = []
    agenda = props.agenda

    return (
        <Swiper {...params} shouldSwiperUpdate>
            {
            agenda.map((item, i) => (
                item ? (
                <div key={i}>
                    <CardAgenda
                        img={item.imagem}
                        link={item.Url}
                        titulo={`${item.titulo}`}
                        data={item.DataEvento.slice(0, 10)}
                        local={item.Campus}
                    />
                </div>
                ) : null
            ))
            }
        </Swiper>
    )
}

export default SliderAgenda;