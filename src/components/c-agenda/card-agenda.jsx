import React, { Component } from 'react'
import styled from 'styled-components'

import iconCalendar from '../../images/icone-calendario.svg';
import iconPin from '../../images/card-pin.svg';

const ImgContainer = styled.div`
    width: 100%;
    height: 229px;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
    align-items: center;
    display: flex;
    align-items: center;
    justify-content: center;

    img{
        max-width: 500px;
    }
`

export default class CardAgenda extends Component {
    render() {
        return (
            <a href={this.props.link} target="_blank">
                <div className="card-agenda">
                    <ImgContainer>
                        <img src={this.props.img} alt="" />
                    </ImgContainer>
                    <div className="texto">
                        <h4 className="titulo">
                            {this.props.titulo}
                        </h4>
                        <div className="data">
                            <img src={iconCalendar} alt=""/>
                            <span>
                                {this.props.data}
                            </span>
                        </div>
                        <div className="local">
                            <img src={iconPin} alt=""/>
                            <span>
                                {this.props.local}
                            </span>
                        </div>
                    </div>
                </div>
            </a>

        )
    }
}
