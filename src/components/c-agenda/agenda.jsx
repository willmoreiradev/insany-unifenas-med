import React, { Component } from 'react'

import ArrowPrev from '../../images/arrow-prev-depo.svg';
import ArrowNext from '../../images/arrow-next-depo.svg';
import SliderAgenda from './slider-agenda';

export default class Agenda extends Component {
    state = {
        agenda: []
    }
    async componentWillMount() {
        const options = {
            method: 'GET',
            headers: new Headers({
                'Content-Type': ' application/json',
                'Authorization': 'Basic QzUxMTBDRkRCMzFGNDU5Nzg2Rjg2MDNBRDUzMTZGNzU6MUREOEU3MDJEQjRCNDEzRUFGRTU2NEUxN0UzM0M5RTY='
            })
        };
        await fetch(`https://intranet.unifenas.br/ApiNoticiaseEventos/v1/eventos`, options)
            .then(response => response.json())
            .then(result => {
                this.setState({ agenda: result.Objeto });
            })
            .catch(err => {
                console.error('Failed retrieving information', err);
            });
    }

    render() {
        return (
            <div className="s-agenda">
                <div className="container">
                    <div className="text">
                        <h1>Agenda</h1>
                        <p>Não fique de fora, confira quais serão os próximos eventos e participe.</p>
                        <a href="https://www.unifenas.br/eventos.asp" target="_blank">Confira todos eventos</a>
                        <div className="ctrl-slide-ag ctrl-slide">
                            <img src={ArrowPrev} className="btn btn-prev" alt=""/>
                            <img src={ArrowNext} className="btn btn-next" alt=""/>
                        </div>
                    </div>
                    <SliderAgenda
                        agenda={this.state.agenda}
                    />
                </div>
            </div>
        )
    }
}
