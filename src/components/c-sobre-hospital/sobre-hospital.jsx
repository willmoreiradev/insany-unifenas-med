import React, { Component } from 'react';

import SliderFotosHospital from './slide-fotos-hospital';

import { Parallax } from 'react-scroll-parallax';

import Coracao from '../../images/img_man_heart.png';
import ArrowPrev from '../../images/arrow-prev-depo.svg';
import ArrowNext from '../../images/arrow-next-depo.svg';

import iconPin from '../../images/icon-location.svg';

class SectionSobreHospital extends Component {
    render() {
        return (
            <section className="s-sobre-hospital">
                <Parallax className="custom-class" y={[-70, -220]} tagOuter="figure" className="coracao">
                    <img src={Coracao} />
                </Parallax>   
                <div className="container">
                    <div className="texto">
                        <h1>{this.props.nome}</h1>
                        <p>
                            {this.props.texto}
                        </p>
                        {this.props.texto2 ? (<p><br/>{this.props.texto2}</p>) : null}
                        <div className="endereco">
                            <img src={iconPin} />
                            <span>
                                {this.props.endereco}
                            </span>
                        </div>
                    </div>
                </div>
                <SliderFotosHospital
                    slidesHosp={this.props.slidesHosp}
                />
                <div className="ctrl-slide-hosp ctrl-slide">
                    <img src={ArrowPrev} className="btn btn-prev" alt=""/>
                    <img src={ArrowNext} className="btn btn-next" alt=""/>
                </div>
            </section>
        );
    }
}

export default SectionSobreHospital;