import React from 'react';
import Swiper from 'react-id-swiper';

const SliderFotosHospital = (props) => {
    const params = {
        navigation: {
            nextEl: '.ctrl-slide-hosp .btn-next',
            prevEl: '.ctrl-slide-hosp .btn-prev'
        },
        slidesPerView: 'auto',
        speed: 800,
        spaceBetween: 0,

        
    }

    let slidesHosp = []
    slidesHosp = props.slidesHosp

    return (
        <Swiper {...params}>
            {
            slidesHosp.map((item, i) => (
                item ? (
                <div key={i}>
                    <div className="foto-hospital">
                        <img src={item.imagem} />
                        <span>{item.descricao}</span>
                    </div>
                </div>
                ) : null
            ))
            }
        </Swiper>
    )
}

export default SliderFotosHospital;