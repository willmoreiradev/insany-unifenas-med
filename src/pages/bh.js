import React, {Component} from "react"

import { Helmet } from "react-helmet";

import { ParallaxProvider } from 'react-scroll-parallax';

import Header from '../components/c-header/header';
import Banner from '../components/c-banner/banner';
import Sobre from '../components/c-sobre/sobre';
import Depoimentos from '../components/c-depoimentos/depoimentos';
import Historia from '../components/c-historia/historia';
import SobreHospital from '../components/c-sobre-hospital/sobre-hospital';
import Tour from "../components/c-tour/tour";

import './../scss/main.scss';

import './../fonts/font.css';
import UniCompleta from "../components/c-uni-completa/uni-completa";
import ActionCards from "../components/c-action-cards/action-cards";
import Novidades from "../components/c-novidades/novidades";
import Agenda from "../components/c-agenda/agenda";
import Contato from "../components/c-contato/contato";
import Footer from "../components/c-footer/footer";
import Mapa from "../components/c-mapa/mapa";

import Aluno from '../images/aluno_circle-foto.png'
import azulEsqDown from '../images/azul-esq-down.svg'
import azulEsqTop from '../images/azul-esq-top.svg'
import azulDirTop from '../images/azul-dir-top.svg'
import bolasCinza from '../images/aluno_cicle-bolinhas2.svg'

import BgBanner from '../images/bg-banner-bh-1.svg';
import DetalhesBanner from '../images/bg-dot.png';

import Image_01_Slide from '../images/banner-bh-1.png';
import Image_02_Slide from '../images/banner-bh-2.png';

import Estudantes from '../images/estudantes-bh.png';

import Bg_01_Slide from '../images/fullbanner.jpg';
import Bg_02_Slide from '../images/fullbanner2.jpg';

import IlustraHomem from '../images/ilustra-fc_01-homem.svg'
import IlustraBg from '../images/ilustra-fc_02-circulo.svg'
import IlustraIcones from '../images/ilustra-fc_03-icones.svg'

import imgNovidade1 from '../images/img-novidade-alfenas-1.jpg';
import imgNovidade2 from '../images/img-novidade-alfenas-2.jpg';
import imgNovidade3 from '../images/img-novidade-alfenas-3.jpg';
import imgNovidade4 from '../images/img-novidade-alfenas-4.jpg';

import imgAgenda from '../images/img-agenda.png';
import imgAgenda2 from '../images/img-agenda-2.png';
import imgAgenda3 from '../images/img-agenda-3.png';

import fotoSlideHosp1 from '../images/UnifenasBH/img-hosp-bh-1.jpg';
import fotoSlideHosp2 from '../images/UnifenasBH/img-hosp-bh-2.jpg';
import fotoSlideHosp3 from '../images/UnifenasBH/img-hosp-bh-3.jpg';
import fotoSlideHosp4 from '../images/UnifenasBH/img-hosp-bh-4.jpg';
import fotoSlideHosp5 from '../images/UnifenasBH/img-hosp-bh-5.jpg';
import fotoSlideHosp6 from '../images/UnifenasBH/img-hosp-bh-6.jpg';
import fotoSlideHosp7 from '../images/UnifenasBH/img-hosp-bh-7.jpg';
import fotoSlideHosp8 from '../images/UnifenasBH/img-hosp-bh-8.jpg';
import fotoSlideHosp9 from '../images/UnifenasBH/img-hosp-bh-9.jpg';
import fotoSlideHosp10 from '../images/UnifenasBH/img-hosp-bh-10.jpg';

import fotoSlideLab1 from '../images/img-lab-bh-1.jpg'
import fotoSlideLab2 from '../images/img-lab-bh-2.jpg'

import Karina from '../images/depoimentos/karina.png'
import Vitor from '../images/depoimentos/vitor.png'
import Alipio from '../images/depoimentos/alipio.png'

import Ladislau from '../images/coordenadores/size-ladislau.png'
import Flavia from '../images/coordenadores/size-flavia.png'
import Aneilde from '../images/coordenadores/size-aneilde.png'
import MobileMenu from "../components/c-mobile-menu/mobile-menu";
import Whatsapp from '../images/whatsapp.svg'
import ModalContato from "../components/c-modal-contato/modal-contato";


let slides = [
  {
    titulo: "Bem-vindo à Medicina Belo Horizonte",
    texto: "Um curso que constrói pessoas aptas a enxergar na vida humana a oportunidade de cuidado.",
    imagem: Estudantes,
  },
  {
      titulo: "Viva uma experiência universitária única",
      texto: "Aqui integração, extensão e pesquisa caminham juntos formando médicos preparados para o mercado.",
      bg: Bg_01_Slide
  },
  {
      titulo: "Experimente momentos incríveis",
      texto: "Com tecnologia de ponta aliada aos mais diversos eventos acadêmicos, você vai viver momentos únicos com a gente.",
      bg: Bg_02_Slide
  }
]

let infoCurso = {
    duracao:"6 anos",
    periodo:"Integral",
    campus:"Belo Horizonte"
}

let depoimentos = [];
depoimentos[0] = {
    avatar: Karina,
    texto: "Olhar para esses seis anos de caminhada na UNIFENAS me faz ter certeza de que eu a escolheria novamente. O método de ensino em espiral, a aprendizagem baseada em problemas e na prática é fácil de se acostumar, ocorre um encantamento, ela te assegura e te da mais confiança. Além do método, nesse tempo bom de muita certeza sobre a escolha do curso e da satisfação de estar nessa instituição, apesar dos percalços e contratempos da vida, eu encontrei nela uma família!",
    nome: "Karina Kopper",
    descricao: "12°período"
}
depoimentos[1] = {
    avatar: Vitor,
    texto: "Meu nome é Vitor Moretto Salomão, curso Medicina na Unifenas BH onde eu sempre quis estudar por ser uma faculdade de excelência e tradição na formação de profissionais, mas também por contar com um time de docentes preparados e sempre a disposição dos alunos. Além disso, o curso conta com uma semiologia que é destaque em todo o Brasil, com laboratórios informatizados e também porque a Unifenas BH trabalha com o plano de ensino PBL que é o ensino baseado em Problemas, na qual traz um diferencial na capacitação do estudante, instigando-o a ser realmente um “estudante”, a Unifenas trabalha com o desenvolvimento do conhecimento em espiral promovendo uma sedimentação maior do conteúdo, por isso sei que vai me capacitar pra sempre continuar estudando mesmo após estar formado que é uma necessidade do profissional médico. Por fim, como aluno da Unifenas estou sendo preparado para o futuro! E hoje a Unifenas BH é minha casa.",
    nome: "Vitor Salomão",
    descricao: "4°período"
}
depoimentos[2] = {
  avatar: Alipio,
  texto: "Acredito que a Unifenas BH foi uma das melhores escolhas que pude fazer. A instituição promove um aprendizado humanizado e nos permite ter uma imersão completa durante todo o curso, com aulas práticas nos laboratórios, UBS e ambulatórios, desenvolvendo em nos, alunos, a evolução a cada semestre da universidade. Formar ótimos médicos não é uma tarefa fácil, além da parte técnica, é necessário o ensino que promova o raciocínio lógico e contextualizado das diferentes possibilidades que encontraremos depois de formado. Para isso, é necessário um corpo docente capacitado e que saiba transmitir esse conhecimento pra os alunos de forma dinâmica — coisa que vemos no cotidiano na Unifenas. Além de todos esses pontos, ainda vemos a coordenação como colaboradores, que nos tratam de forma empática e atenciosa.",
  nome: "Alípio Prado",
  descricao: "3°período"
}

let novidades = []
novidades[0] = {
  imagem: imgNovidade1,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}
novidades[1] = {
  imagem: imgNovidade2,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}
novidades[2] = {
  imagem: imgNovidade3,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}
novidades[3] = {
  imagem: imgNovidade4,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}

let agenda = []
agenda[0] = {
  imagem: imgAgenda2,
  titulo: "V Simpósio da liga de Clínica Cirurgia",
  data: "03 de outubro de 2019",
  local: "Salão de Eventos, 19 horas Câmpus Alfenas"
}
agenda[1] = {
  imagem: imgAgenda3,
  titulo: "Atlética de Ciências Contábeis doa kits ",
  data: "03 de outubro de 2019",
  local: "Salão de Eventos, 19 horas Câmpus Alfenas"
}
agenda[2] = {
  imagem: imgAgenda,
  titulo: "Colação de grau em separado",
  data: "03 de outubro de 2019",
  local: "Salão de Eventos, 19 horas Câmpus Alfenas"
}

let slidesHosp = [
  {
     imagem : fotoSlideHosp1,
     descricao : "Consultorio do CEASC"
  },
  {
     imagem : fotoSlideHosp4,
     descricao : "Sala do Bloco Cirurgico do CEASC"
  },

  {
     imagem : fotoSlideHosp7,
     descricao : "Ambulatório do CEASC"
  },
  {
    imagem : null,
    descricao : null
  },
]

let slidesLab = [
  {
    imagem : fotoSlideHosp2,
    descricao : "Sala de Comunicação do centro de treinamento de Habilidades"
 },
 {
    imagem : fotoSlideHosp3,
    descricao : "Sala de Grupo Tutorial"
 },
 {
  imagem : fotoSlideHosp5,
  descricao : "Sala do Centro de Treinamento de Habilidiades"
  },
  {
    imagem : fotoSlideHosp6,
    descricao : "Centro de Informática"
  },
  {
    imagem : fotoSlideHosp8,
    descricao : "Laboratório de Morfologia"
  },
  {
    imagem : fotoSlideHosp9,
    descricao : "Sala do Centro de Treinamento de Habilidades"
  },
  {
    imagem : fotoSlideHosp10,
    descricao : "Laboratório de Anatomia"
  },
  {
    imagem : null,
    descricao : null
  },
]

let coordenacao = []
coordenacao[0] = {
  imagem : Ladislau,
  funcao : "Coordenação",
  nome: "Prof. Ms. Ladislau José Fernandes Júnior"
}
coordenacao[1] = {
  imagem : Flavia,
  funcao : "Coordenação Adjunta",
  nome: "Profª. Ms. Flávia Pereira de Freitas Junqueira"
}
coordenacao[2] = {
  imagem : Aneilde,
  funcao : "Coordenação Adjunta",
  nome: "Profa. Ms. Aneilde Maria Ribeiro de Brito"
}

let periodos = [];
periodos[0] = '1º período - 4º período'
periodos[1] = '5º período - 8º período'
periodos[2] = '9º período - 12º período'

class MedBh extends Component {
  render() { 

    return (  
      <ParallaxProvider>
        <div className="main">
          <Helmet>
            <title>Medicina UNIFENAS - Seja Bem-vindo</title>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" />  
            <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet" />
          </Helmet>
          <Header
            path={this.props.location.pathname}
            whatsapp="https://api.whatsapp.com/send?phone=553597049384" 
          />
          <MobileMenu 
            path={this.props.location.pathname}
            whatsapp="https://api.whatsapp.com/send?phone=553597049384" 
          />
          <Banner 
            bg={BgBanner}
            bg1={Bg_01_Slide}
            bg2={Bg_02_Slide}
            path={this.props.location.pathname}
            dots={DetalhesBanner}
            slides={slides}
            infoCurso={infoCurso}
            btn='amarelo'
          >
            <p>Pelo 2º ano consecutivo, eleito <strong>melhor curso privado</strong> de Minas Gerais</p>
          </Banner>
          <Sobre
            videoLink='https://www.youtube.com/watch?v=inCl7IAfr8U'
            videoDescricao='Medicina Unifenas - Câmpus Belo Horizonte'
            preco="8.761,60"
            textoLab="Laboratório de Treinamento de Habilidades Clínicas, treinando habilidades de comunicação com atores que desempenham papel de “pacientes padronizados”, em consultas simuladas ou por meio de modelos e peças apropriadas para simulação de manobras de exame físico e de situações clínicas específicas."
            slidesLab={slidesLab}
            coordenacao={coordenacao}
            periodos={periodos}
            infoCurso={infoCurso}
            path={this.props.location.pathname} 
            cidade="bh"
          />
          <Depoimentos
            depoimentos={depoimentos}
          />
          <Historia/>
          <SobreHospital
            nome="CEASC"
            texto="O CEASC é referência para atendimento em especialidades médicas aos moradores da regional Norte de BH. As especialidades médicas ofertadas a esta população incluem: cardiologia, pneumologia, gastroenterologia, infectologia, urologia, hematologia, dermatologia, endocrinologia, neurologia, otorrinolaringologia, ortopedia e cirurgia ambulatorial."
            texto2="O CEASC abriga o Centro de Especialidades Médicas da Regional Norte e realiza exames especializados. São 2.000 consultas e 300 exames por mês. "
            endereco="Rua Líbano, 66 - Bairro Itapoã - CEP: 31710-030"
            slidesHosp={slidesHosp}
          />
          <Tour
            cidade="bh"
          />
          <UniCompleta
            logo={false}
            titulo="Método de ensino - PBL"
            texto="O PBL (Problem Based Learning) tem seu ensino baseado na resolução de problemas e é o método que vem sendo aplicado na faculdade de medicina de Belo Horizonte, incentivando os alunos a buscarem sempre conhecimentos além das salas de aula."
            img={Aluno}
            azulEsqDown={azulEsqDown}
            azulEsqTop={azulEsqTop}
            azulDirTop={azulDirTop} 
            bolasCinza={bolasCinza}
            cidade="bh"         
            topAfter={-47}
          />
          <ActionCards
            path={this.props.location.pathname}
          />
          <Novidades
            novidades={novidades}
          />
          <Agenda
            agenda={agenda}
          />
          <Contato
            img={IlustraHomem}
            imgBase={IlustraBg}
            imgAfter={IlustraIcones}
            topAfter={-165}

            whatsapp="https://api.whatsapp.com/send?phone=553597049384"
            email="medicina.bh@unifenas.br"
            telefone="(31) 2536-5681"
            actionTel="+553125365682"
          />
          <Mapa
            mapa="https://insanydesign.com/projetos/unifenas/mapas/bh/"
            id="mapa"
          />
          <Footer />
          <a className="wpp-btn" href="https://api.whatsapp.com/send?phone=553597049384" target="_blank">
            <img src={Whatsapp} />
          </a>
          <ModalContato />
          <div className="overlay"></div>
        </div>
      </ParallaxProvider>
    );
  }
}
 
export default MedBh;
