import React, {Component} from "react"

import { Helmet } from "react-helmet";

import { ParallaxProvider } from 'react-scroll-parallax';

import Header from '../components/c-header/header';
import Banner from '../components/c-banner/banner';
import Sobre from '../components/c-sobre/sobre';
import Depoimentos from '../components/c-depoimentos/depoimentos';
import Historia from '../components/c-historia/historia';
import SobreHospital from '../components/c-sobre-hospital/sobre-hospital';
import Tour from "../components/c-tour/tour";

import './../scss/main.scss';

import './../fonts/font.css';
import UniCompleta from "../components/c-uni-completa/uni-completa";
import ActionCards from "../components/c-action-cards/action-cards";
import Novidades from "../components/c-novidades/novidades";
import Agenda from "../components/c-agenda/agenda";
import Contato from "../components/c-contato/contato";
import Footer from "../components/c-footer/footer";
import Mapa from "../components/c-mapa/mapa";
import MobileMenu from "../components/c-mobile-menu/mobile-menu";


import Whatsapp from '../images/whatsapp.svg'

import Aluna from '../images/aluna_circle-foto.png'
import azulEsqDown from '../images/azul-esq-down.svg'
import azulEsqTop from '../images/azul-esq-top.svg'
import azulDirTop from '../images/azul-dir-top.svg'
import CardsAfter from '../images/aluna_circle-cards.svg'

import BgBanner from '../images/bg-banner-alfenas-1.svg';
import DetalhesBanner from '../images/bg-dot.png';


import Image_01_Slide from '../images/banner-alfenas-1.png';
import Image_02_Slide from '../images/banner-alfenas-2.png';

import Bg_01_Slide from '../images/fullbanner.jpg';
import Bg_02_Slide from '../images/fullbanner2.jpg';

import Estudantes from '../images/estudantes-alfenas.png';

import IlustraMulher from '../images/ilustra-fc_01-mulher.svg'
import IlustraBg from '../images/ilustra-fc_02-circulo.svg'
import IlustraIcones from '../images/ilustra-fc_03-icones.svg'

import imgNovidade1 from '../images/img-novidade-alfenas-1.jpg';
import imgNovidade2 from '../images/img-novidade-alfenas-2.jpg';
import imgNovidade3 from '../images/img-novidade-alfenas-3.jpg';
import imgNovidade4 from '../images/img-novidade-alfenas-4.jpg';

import imgAgenda from '../images/img-agenda.png';
import imgAgenda2 from '../images/img-agenda-2.png';
import imgAgenda3 from '../images/img-agenda-3.png';

import fotoSlideHosp1 from '../images/UnifenasAlfenas/img-hosp-alfenas-1.jpg';
import fotoSlideHosp2 from '../images/UnifenasAlfenas/img-hosp-alfenas-2.jpg';
import fotoSlideHosp3 from '../images/UnifenasAlfenas/img-hosp-alfenas-3.jpg';
import fotoSlideHosp4 from '../images/UnifenasAlfenas/img-hosp-alfenas-4.jpg';
import fotoSlideHosp5 from '../images/UnifenasAlfenas/img-hosp-alfenas-5.jpg';
import fotoSlideHosp6 from '../images/UnifenasAlfenas/img-hosp-alfenas-6.jpg';
import fotoSlideHosp7 from '../images/UnifenasAlfenas/img-hosp-alfenas-7.jpg';
import fotoSlideHosp8 from '../images/UnifenasAlfenas/img-hosp-alfenas-8.jpg';
import fotoSlideHosp9 from '../images/UnifenasAlfenas/img-hosp-alfenas-9.jpg';
import fotoSlideHosp10 from '../images/UnifenasAlfenas/img-hosp-alfenas-10.jpg';
import fotoSlideHosp11 from '../images/UnifenasAlfenas/img-hosp-alfenas-11.jpg';
import fotoSlideHosp12 from '../images/UnifenasAlfenas/img-hosp-alfenas-12.jpg';
import fotoSlideHosp13 from '../images/UnifenasAlfenas/img-hosp-alfenas-13.jpg';

import fotoSlideLab1 from '../images/img-novidade-alfenas-4.jpg'
import fotoSlideLab2 from '../images/img-novidade-alfenas-4.jpg'

import Murilo from '../images/depoimentos/murilo.png';
import Suzy from '../images/depoimentos/suzy.png';
import MariaClara from '../images/depoimentos/maria-clara.png';
import JoaoPedro from '../images/depoimentos/joao-pedro.png';
import Monique from '../images/depoimentos/monique.png';
import Lais from '../images/depoimentos/lais.png';
import Leo from '../images/depoimentos/leo.png';
import Thulio from '../images/depoimentos/Thulio.png';
import Isadora from '../images/depoimentos/isadora.png';
import Guilherme from '../images/depoimentos/Guilherme.png';
import Mariana from '../images/depoimentos/mariana.png';

import Annie from '../images/coordenadores/Size-Annie_Beatriz.jpg';
import Luisa from '../images/coordenadores/Size-Luisa.jpg';
import ModalContato from "../components/c-modal-contato/modal-contato";

let slides = [
  {
    titulo: "Bem-vindo à Medicina Alfenas",
    texto: "Um curso que constrói pessoas aptas a enxergar na vida humana a oportunidade de cuidado.",
    imagem: Estudantes,
  },
  {
      titulo: "Viva uma experiência universitária única",
      texto: "Aqui integração, extensão e pesquisa caminham juntos formando médicos preparados para o mercado.",
      bg: Bg_01_Slide
  },
  {
      titulo: "Experimente momentos incríveis",
      texto: "Com tecnologia de ponta aliada aos mais diversos eventos acadêmicos, você vai viver momentos únicos com a gente.",
      bg: Bg_02_Slide
  }
]

let infoCurso = {
  duracao:"6 anos",
  periodo:"Integral",
  campus:"Alfenas"
}

let depoimentos = [];
depoimentos[0] = {
    avatar: Murilo,
    texto: "O senhor Deus sempre trilhou meus caminhos e dirigiu meus passos nas melhores escolhas. Uma delas foi me integrar na Unifenas para me estruturar um excelente médico.",
    nome: "Murilo Monroe Mota",
    descricao: "5°período"
}
depoimentos[1] = {
    avatar: Suzy,
    texto: "Posso dizer que eu não escolhi medicina, a medicina me escolheu. O momento em que me dei conta de que seria médica foi aquele em que me senti mais incapaz de ajudar uma pessoa amada, somente um médico poderia ajudar. A partir daquele momento meu sonho era ser médica, tratar as pessoas com dignidade e carinho. Quando ingressei na Unifenas tive a certeza de que amava a medicina, pois a cada aula me sinto mais capaz e preparada para lidar com o bem mais precioso que um ser humano possui: A vida.",
    nome: "Suzy Mayumi Freire Ciosak",
    descricao: "3° período"
}
depoimentos[2] = {
  avatar: MariaClara,
  texto: "Medicina é um sonho de infância, ao conquistar a vaga desse curso na Unifenas percebi que a minha jornada para realização desse sonho não seria fácil, no entanto, posso afirmar que está sendo com qualidade, com oportunidade de aprendizado por excelentes professores e com muitas experiências práticas para um formação competente e humana. Me orgulho de estudar nessa Universidade e levarei os ensinamentos de grandes mestres para toda a vida.",
  nome: "Maria Clara Gontijo dos Santos",
  descricao: "6° período"
}
depoimentos[3] = {
  avatar: JoaoPedro,
  texto: "No mesmo ano que entrei no curso de Medicina da Unifenas, ele foi avaliado como uns dos melhores de Minas Gerais, fiquei muito surpreso e feliz, e hoje, posso confirmar e vivenciar cada momento dessa conquista, não só nas salas de aula com grandes mestres para compartilhar seus conhecimentos, mas também no hospital escola, onde os profissionais da saúde presentes, inclusive os médicos, estão a disposição para atender as demandas dos alunos. Estou muito feliz por estar cursando medicina aqui na Unifenas, me surpreendo pelo enorme aprendizado médico a cada período que passa e ainda espero muitas realizações ao longo do curso, que me deixa cada dia mais perto do sonho de, não apenas ser um ótimo médico, mas também, ser uma pessoa melhor.",
  nome: "João Pedro Tavares da Silva",
  descricao: "6° período"
}
depoimentos[4] = {
  avatar: Monique,
  texto: "O curso de Medicina da Unifenas/Alfenas para mim é um divisor de águas no futuro profissional de qualquer acadêmico. Para chegar até aqui foram alguns anos de cursinho pré-vestibular, fracassos, mas sobretudo resiliência, e enfim a vitória. Olhando pra trás hoje, vejo que me orgulho muito de chegar aonde cheguei e não poderia ter feito melhor escolha para a minha vida. Como alunos, é notável a nossa evolução ano após ano, o despertar de um sentimento de empatia cada vez maior com os pacientes e enfermos e a construção de uma maturidade tanto emocional quanto conteudista. Os eixos temáticos das disciplinas são interligados entre si, as metodologias ativas proporcionam autonomia e autodidatismo ao aluno, e considero ainda a excelência dos mestres que lecionam, nos proporcionando um aprendizado ilimitado. As modalidades de Ensino, Pesquisa e Extensão abrem um leque de oportunidades para o aluno que queira se diferenciar no mercado de trabalho. Ademais, o nosso hospital-escola (HUAV) é referência na região em assistência integral, ensino e pesquisa. A cada ano formam-se mais médicos e a concorrência por uma boa posição no mercado torna-se realidade, mas eu tenho convicção de que como acadêmica do curso de Medicina da Unifenas/Alfenas essa trajetória ficará muito mais prazerosa e o sucesso será certeiro.",
  nome: "Monique Angela Freire Carciliano",
  descricao: "5° período"
}
depoimentos[5] = {
  avatar: Lais,
  texto: "Medicina: um sonho que hoje se transformou em realidade! Hoje, cursando o segundo ano do curso de Medicina, só tenho o que agradecer por tudo que a Unifenas tem me proporcionado. Professores competentes e extremamente compromissados com o nosso ensino. Professores amigos, companheiros e incentivadores da realização de projetos e de pesquisas. Coordenadores que não medem esforços para estarem do nosso lado e nos proporcionar o mais alto grau de satisfação com o curso. Mesmo muito longe de casa, tendo que abdicar da família e cidade natal, tenho a certeza que a Unifenas faz e fará parte da melhor fase da minha vida. Medicina Unifenas, a responsável pelo início de uma vida profissional, que antes era apenas um sonho!",
  nome: "Lais Roncato de Carvalho Alves",
  descricao: "4° período"
}
depoimentos[6] = {
  avatar: Leo,
  texto: "Ter em mente a consciência de cursar medicina é uma decisão desafiadora, tornando-se um foco pelo qual lutamos e vivemos em busca de recursos em prol da vida. A Unifenas nos proporciona a oportunidade de vivenciar diariamente esse objetivo e o tempo nos direciona a um futuro próximo e seguramente comprometido com o bem do ser humano, nos preparando não somente de forma técnica e profissional, mas essencialmente nos preocupando com a pessoa em sua integralidade. Professores capacitados, ambiente muito bem estruturado, metodologias ativas e outros atributos nos são oferecidos para melhor experiência com a medicina, o que nos dá a certeza de uma boa preparação e capacitação para a aplicação na profissão",
  nome: "Léo Pedro Rufino",
  descricao: "2° período"
}
depoimentos[7] = {
  avatar: Thulio,
  texto: "Passar em medicina é um sonho que se realizou! Agora, só tenho que agradecer a Unifenas pela excelência de seu ensino e pelo apoio que ela vem me oferecendo, me dando a segurança de que estarei realmente preparado para a minha vida profissional.",
  nome: "Thulio Willen Siqueira Neder",
  descricao: "3° período"
}
depoimentos[8] = {
  avatar: Isadora,
  texto: "Escolhi a Unifenas pelo seu tradicionalismo e ótima infraestrutura. Além disso, pela quantidade de excelentes profissionais conhecidos formados na universidade. A Unifenas tem atendido todas as minhas expectativas e me coloca em contato com a prática médica desde o primeiro período!",
  nome: "Isadora Banhos",
  descricao: "2° período"
}
depoimentos[9] = {
  avatar: Guilherme,
  texto: "Escolhi a Unifenas por ser a melhor das particulares de Minas. Mesmo estando no primeiro período já me apaixonei pelo curso e pela faculdade.",
  nome: "Guilherme Henrique dos Santos Silva",
  descricao: "1° período"
}
depoimentos[10] = {
  avatar: Mariana,
  texto: "Assim como diversas profissões a medicina é uma profissão muito nobre, salvar vidas é muito valioso , por isso o processo de escolha da instituição de ensino tem que ser extremamente preciso e valorizado. A minha escolha da Unifenas foi pautada em um infraestrutura excepcional e diferenciada no Brasil , além de uma equipe de profissionais extremamente qualificados que transmitem ensinamentos , confiança e conforto emocional . Estar aqui e fazer parte dessa história é ter a certeza que estou no caminho certo para a concretização de um grande sonho.",
  nome: "Mariana Fonseca Meireles",
  descricao: "1° período"
}


let novidades = []
novidades[0] = {
  imagem: imgNovidade1,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}
novidades[1] = {
  imagem: imgNovidade2,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}
novidades[2] = {
  imagem: imgNovidade3,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}
novidades[3] = {
  imagem: imgNovidade4,
  data: "19 jul 2019",
  titulo: "Espaço Formatura Medicina Alfenas"
}

let agenda = []
agenda[0] = {
  imagem: imgAgenda2,
  titulo: "V Simpósio da liga de Clínica Cirurgia",
  data: "03 de outubro de 2019",
  local: "Salão de Eventos, 19 horas Câmpus Alfenas"
}
agenda[1] = {
  imagem: imgAgenda3,
  titulo: "Atlética de Ciências Contábeis doa kits ",
  data: "03 de outubro de 2019",
  local: "Salão de Eventos, 19 horas Câmpus Alfenas"
}
agenda[2] = {
  imagem: imgAgenda,
  titulo: "Colação de grau em separado",
  data: "03 de outubro de 2019",
  local: "Salão de Eventos, 19 horas Câmpus Alfenas"
}

let slidesHosp = [
 {
    imagem : fotoSlideHosp1,
    descricao : "UTI Pediátrica do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp2,
    descricao : "Clínica de fisioterapia do CER"
 },
 {
    imagem : fotoSlideHosp3,
    descricao : "Ressonância Nuclear Magnética do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp4,
    descricao : "Clínica de Fonoaudiologia do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp5,
    descricao : "Recepção do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp6,
    descricao : "Fachada do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp7,
    descricao : "Maternidade do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp8,
    descricao : "Ultrassonografia do Hospital Universitário Alzira Velano"
 },
 {
    imagem : fotoSlideHosp9,
    descricao : "Fachada do Hospital Universitário Alzira Velano "
 },
  {
    imagem : fotoSlideHosp10,
    descricao : "Fachada do Hospital Universitário Alzira Velano "
  },
  {
    imagem : fotoSlideHosp11,
    descricao : "Entrada do pronto socorro do Hospital Universitário Alzira Velano"
  },
  {
    imagem : fotoSlideHosp12,
    descricao : "Fachada do Hospital Universitário Alzira Velano "
  },
  {
    imagem : null,
    descricao : null
  },
]

let slidesLab = []
slidesLab[0] = {
  imagem : fotoSlideHosp13,
  descricao : "Laboratório Morfofuncional"
}
slidesLab[1] = {
  imagem : null,
  descricao : null
}

let coordenacao = []
coordenacao[0] = {
  imagem : Annie,
  funcao : "Coordenação",
  nome: "Prof.ª Ms. Annie Beatriz de Carvalho"
}
coordenacao[1] = {
  imagem : Luisa,
  funcao : "Coordenação Adjunta",
  nome: "Prof.ª Ms. Luisa Barbosa Messora"
}

let periodos = [];
for(let i=0; i<12; i++){
  periodos[i] = `${i+1}º período`;
}

class MedAlfenas extends Component {
  render() { 
    return (  
      <ParallaxProvider>
        <div className="main">
          <Helmet>
            <title>Medicina UNIFENAS - Seja Bem-vindo</title>
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css" /> 
            <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet" /> 
          </Helmet>
          <Header 
            path={this.props.location.pathname} 
            whatsapp="https://api.whatsapp.com/send?phone=553597049384" 
          />
          <MobileMenu 
            path={this.props.location.pathname} 
            whatsapp="https://api.whatsapp.com/send?phone=553597049384" 
          />
          <Banner 
            bg={BgBanner}
            bg1={Bg_01_Slide}
            bg2={Bg_02_Slide}
            dots={DetalhesBanner}
            slides={slides}
            infoCurso={infoCurso}
            path={this.props.location.pathname}
            btn='amarelo'
          >
            <p>Pelo 2º ano consecutivo, eleito <strong>melhor curso privado</strong> de Minas Gerais</p>
          </Banner>
          <Sobre
            videoLink='https://www.youtube.com/watch?v=inCl7IAfr8U'
            videoDescricao='Medicina Unifenas - Câmpus Alfenas'
            preco="8.138,18"
            textoLab="O curso conta com laboratórios básicos e especializados que se destinam à realização das atividades práticas necessárias ao desenvolvimento de competências e habilidades essenciais à formação do profissional médico."
            slidesLab={slidesLab}
            coordenacao={coordenacao}
            periodos={periodos}
            infoCurso={infoCurso}
            path={this.props.location.pathname} 
            cidade="alfenas"
          />
          <Depoimentos
            depoimentos={depoimentos}
          />
          <Historia/>
          <SobreHospital
            nome="Hospital-escola de referência"
            texto="O Hospital oferece Residência Médica em 12 especialidades: Anestesiologia, Clínica Médica, Cirurgia Geral, Ginecologia e Obstetrícia, Medicina de Família e Comunidade, Medicina Intensiva, Medicina de Urgência, Neurocirurgia, Nefrologia, Ortopedia e Traumatologia, Pediatria e Radiologia e diagnóstico por imagem. Na área odontológica, o hospital possui curso de especialização em cirurgia bucomaxilofacial."
            endereco="R. Geraldo Freitas da Costa, 120, Cruz Preta, Alfenas"
            slidesHosp={slidesHosp}
          />
          <Tour
            cidade="alfenas"
          />
          <UniCompleta
            logo={true}
            titulo="Uma universidade completa"
            texto="Nós queremos ser mais que instituição de ensino, queremos ser parceiros e te incentivar a participar de tudo o que agregue a sua trajetória de aprendizado."
            img={Aluna}
            azulEsqDown={azulEsqDown}
            azulEsqTop={azulEsqTop}
            azulDirTop={azulDirTop}
            imgCards={CardsAfter}
            cidade="al"
            topAfter={-47}
          />
          <ActionCards
            path={this.props.location.pathname}
          />
          <Novidades
            novidades={novidades}
          />
          <Agenda
            agenda={agenda}
          />
          <Contato
            img={IlustraMulher}
            imgBase={IlustraBg}
            imgAfter={IlustraIcones}
            topAfter={-165}

            whatsapp="https://api.whatsapp.com/send?phone=553597049384"
            email="medicina.alfenas@unifenas.br"
            telefone="(35) 3299-3166"
            actionTel="+553532993166"
          />
          <Mapa
            mapa="https://insanydesign.com/projetos/unifenas/mapas/alfenas/"
            id="mapa"
          />
          <Footer/>
          <a className="wpp-btn" href="https://api.whatsapp.com/send?phone=553597049384" target="_blank">
            <img src={Whatsapp} />
          </a>
          <ModalContato />
          <div className="overlay"></div>
        </div>
      </ParallaxProvider>
    );
  }
}
 
export default MedAlfenas;
